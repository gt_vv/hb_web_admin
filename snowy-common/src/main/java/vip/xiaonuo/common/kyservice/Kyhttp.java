package vip.xiaonuo.common.kyservice;


import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Indexed;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Component
public class Kyhttp {


    @Value("${kyservice.address}")
    private String kyaddress;

    @Value("${kyservice.appId}")
    private String appId;

    @Value("${kyservice.appSecret}")
    private String appSecret;


    //获取登录令牌
    public String getToken() {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("appId", appId);
        paramMap.putOnce("appSecret", appSecret);
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_authenticate)
                .header(Header.CONTENT_TYPE, "application/json")
                .body(paramMap.toString())
                .execute().body();
        String accessKey = JSONUtil.parseObj(result2).getJSONObject("data").getStr("accessKey");
        return accessKey;
    }

    /**
     * 获取所有计算节点的信息
     */
    public JSONObject getAllNode() {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_node_list)
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2);
    }


    /**
     * 获取算法列表
     *
     * @return
     */
    public JSONObject getAlg() {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_algorithm)
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2);
    }


    /**
     * 创建算法任务
     *
     * @return
     */
    public Integer addTask(String taskName) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("name", taskName);
        paramMap.putOnce("type", 1);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_task_edit_name)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        //System.out.println(result2);
        return JSONUtil.parseObj(result2).getJSONObject("data").getInt("taskId");
    }

    /**
     * 创建算法任务
     *
     * @return
     */
    public Integer editTask(Integer id, String taskName) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("taskId", id);
        paramMap.putOnce("name", taskName);
        paramMap.putOnce("type", 1);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_task_edit_name)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        //System.out.println(result2);
        return JSONUtil.parseObj(result2).getJSONObject("data").getInt("taskId");
    }

    /**
     * 给任务关联算法
     *
     * @return
     */
    public JSONObject taskAlg(Integer taskId, Set<String> algList) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("taskId", taskId);
        paramMap.putOnce("algorithmList", algList);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_task_edit_algorithm)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        return JSONUtil.parseObj(result2);
    }


    /**
     * 给任务算法添加摄像头
     *
     * @param taskId
     * @param algorithm
     * @param cameraList
     * @return
     */
    public JSONObject taskAlgAndCamare(Integer taskId, String algorithm, List<String> cameraList) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("taskId", taskId);
        paramMap.putOnce("algorithm", algorithm);
        Set<String> cameraSet = new HashSet<>();
        cameraSet.addAll(cameraList);
        paramMap.putOnce("cameraList", cameraSet);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_task_edit_camera)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        return JSONUtil.parseObj(result2);
    }

    /**
     * 添加摄像头
     *
     * @return
     */
    public String addCamera(String name, String stream) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("name", name);
        paramMap.putOnce("stream", stream);
        paramMap.putOnce("streamUrl", stream);
        paramMap.putOnce("type", 1);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_camera_edit)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        Boolean bool = JSONUtil.parseObj(result2).getJSONObject("requestInfo").getBool("flag");
       if(bool){
           //添加成功
           return "";
       }else{
           //添加失败
           return JSONUtil.parseObj(result2).getJSONObject("data").getStr("errorMsg");
       }
    }

    public String editCamera(Integer id,String name, String stream) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("name", name);
        paramMap.putOnce("stream", stream);
        paramMap.putOnce("id", id);
        paramMap.putOnce("streamUrl", stream);
        paramMap.putOnce("type", 1);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_camera_edit)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        Boolean bool = JSONUtil.parseObj(result2).getJSONObject("requestInfo").getBool("flag");
        if(bool){
            //添加成功
            return "";
        }else{
            //添加失败
            return JSONUtil.parseObj(result2).getJSONObject("data").getStr("errorMsg");
        }
    }



    public JSONArray getCameraByKey(String keyword) {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_camera_list + "?keyword="+ keyword)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2).getJSONObject("data").getJSONArray("cameraList");

    }

    public JSONArray getCamera() {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_camera_list )
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2).getJSONObject("data").getJSONArray("cameraList");

    }

    public JSONArray getTaskList() {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_task_list)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2).getJSONObject("data").getJSONArray("taskList");

    }




    /**
     * 获取任务详情
     *
     * @return
     */
    public JSONArray getTaskDetail(Integer taskId) {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_task_detail + "?taskId=" + taskId)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        System.out.println(result2);
        return JSONUtil.parseObj(result2).getJSONObject("data").getJSONObject("config").getJSONArray("taskList");

    }



    public JSONObject startTask(Integer taskId) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("taskId", taskId);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_task_start)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        return JSONUtil.parseObj(result2);
    }

    public JSONObject stopTask(Integer taskId) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("taskId", taskId);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_task_stop)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        return JSONUtil.parseObj(result2);

    }

    /**
     * 获取摄像头列表
     *
     * @param indexCode
     * @return
     */
    public JSONObject getCameraList(String indexCode) {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_camera_list + "?keyword=" + indexCode)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2);

    }

    /**
     * 获取回调列表
     * @return
     */
    public JSONObject getCallbackList() {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_callback_list)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2).getJSONObject("data");

    }

    /**
     * 任务关联回调
     * @return
     */
    public JSONObject editCallback(Integer taskId,Integer callbackId,Integer interval,Integer content) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("taskId", taskId);
        paramMap.putOnce("callbackId", callbackId);
        paramMap.putOnce("interval", interval);
        paramMap.putOnce("content", content);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_task_edit_callback)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        return JSONUtil.parseObj(result2);

    }

    /**
     * 获取未分组的摄像头
     * @return
     */
    public JSONObject getCreamBySub() {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_camera_record_sub  + "?parentId=-1")
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2);

    }


    public JSONObject getConsoleNodes() {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_console_nodes  + "?parentId=-1")
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2).getJSONObject("data");

    }

    public JSONObject getConsoleServerStatus() {
        String token = getToken();
        String result2 = HttpRequest.get(kyaddress + KyApi.openapi_console_server_status  + "?parentId=-1")
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .execute().body();
        return JSONUtil.parseObj(result2).getJSONObject("data");

    }


    public JSONObject cameraRefresh(Integer id) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("cameraId", id);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_camera_refresh)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        return JSONUtil.parseObj(result2);

    }

    public JSONObject taskDelete(Integer id) {
        JSONObject paramMap = new JSONObject();
        paramMap.putOnce("taskId", id);
        String token = getToken();
        String result2 = HttpRequest.post(kyaddress + KyApi.openapi_task_delete)
                .header(Header.CONTENT_TYPE, "application/json")
                .header("crKey", token)
                .body(paramMap.toString())
                .execute().body();
        return JSONUtil.parseObj(result2);

    }

}
