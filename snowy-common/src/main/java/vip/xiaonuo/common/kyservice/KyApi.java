package vip.xiaonuo.common.kyservice;

public class KyApi {
    /**
     * auth 验证
     */
    public static final String openapi_authenticate = "/api/openapi/authenticate";

    /**
     * 所有节点列表
     */
    public static final String openapi_console_nodes = "/api/openapi/console/nodes";

    /**
     * 智能计算节点
     * 查询所有节点信息
     */
    public static final String openapi_node_list = "/api/openapi/node/list";

    /**
     * 获取可用算法
     */
    public static final String openapi_algorithm = "/api/openapi/algorithm";

    /**
     * 创建或修改任务名称
     */
    public static final String openapi_task_edit_name = "/api/openapi/task/edit/name";

    /**
     * 修改任务关联算法
     */
    public static final String openapi_task_edit_algorithm = "/api/openapi/task/edit/algorithm";

    /**
     * 修改任务算法关联的摄像头
     */
    public static final String openapi_task_edit_camera = "/api/openapi/task/edit/camera";

    /**
     * 新增或编辑摄像头点位
     */
    public static final String openapi_camera_edit = "/api/openapi/camera/edit";

    /**
     * 获取摄像头列表
     */
    public static final String openapi_camera_list = "/api/openapi/camera/list";

    /**
     * 获取任务列表
     */
    public static final String openapi_task_list = "/api/openapi/task/list";

    /**
     * 获取任务详情
     */
    public static final String openapi_task_detail = "/api/openapi/task/detail";

    /**
     * 获取摄像头录像详情
     */
    public static final String openapi_camera_record_detail = "/api/openapi/camera/record/detail";

    /**
     * 开启任务
     */
    public static final String openapi_task_start = "/api/openapi/task/start";

    /**
     * 关闭任务
     */
    public static final String openapi_task_stop = "/api/openapi/task/stop";

    public static final String openapi_callback_list = "/api/openapi/callback/list";


    public static final String openapi_task_edit_callback= "/api/openapi/task/edit/callback";



    public static final String openapi_camera_record_sub = "/api/camera/record/sub";

    public static final String openapi_console_server_status = "/api/openapi/console/server/status";

    /**
     * 摄像头刷新
     */
    public static final String openapi_camera_refresh = "/api/openapi/camera/refresh";

    /**
     * 删除任务
     */
    public static final String openapi_task_delete = "/api/openapi/task/delete";




}
