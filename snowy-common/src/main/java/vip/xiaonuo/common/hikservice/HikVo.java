package vip.xiaonuo.common.hikservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/1 17:50
 */
public class HikVo {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class pageVo {
        /**
         * 页码
         */
        @NotEmpty
        private Integer pageNo;
        /**
         * 条数
         */
        @NotEmpty
        private Integer pageSize;
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ResourcePageVo extends pageVo {
        /**
         * 资源名称
         */
        private String name;

        /**
         * 区域编码
         */
        private String[] regionIndexCodes;

        /**
         * 是否搜索子区域
         */
        private Boolean isSubRegion;

        /**
         * 资源类型 camera
         */
        private String resourceType;
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ResourceLineVo {
        /**
         * 资源名称
         */
        private String[] indexCode;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static  class CameraUrlVo{
        /**
         * 监控点编号
         */
        private String cameraIndexCode;

        /**
         * 取流协议
         * “hik”:HIK私有协议，使用视频SDK进行播放时，传入此类型；
         * “rtsp”:RTSP协议；
         * “rtmp”:RTMP协议（RTMP协议只支持海康SDK协议、EHOME协议、ONVIF协议接入的设备；只支持H264视频编码和AAC音频编码）；
         * “hls”:HLS协议（HLS协议只支持海康SDK协议、EHOME协议、ONVIF协议接入的设备；只支持H264视频编码和AAC音频编码）；
         * “ws”:Websocket协议（一般用于H5视频播放器取流播放）。
         */
        private String protocol;
    }

    /**
     * 摄像头控制
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CameraController{
        /**
         *  监控点编号
         */
        private String cameraIndexCode;

        /**
         * 0-开始 ，1-停止
         */
        private Integer action;

        /**
         * 不区分大小写
         * 说明：
         * LEFT 左转
         * RIGHT右转
         * UP 上转
         * DOWN 下转
         * ZOOM_IN 焦距变大
         * ZOOM_OUT 焦距变小
         * LEFT_UP 左上
         * LEFT_DOWN 左下
         * RIGHT_UP 右上
         * RIGHT_DOWN 右下
         * FOCUS_NEAR 焦点前移
         * FOCUS_FAR 焦点后移
         * IRIS_ENLARGE 光圈扩大
         * IRIS_REDUCE 光圈缩小
         * WIPER_SWITCH 接通雨刷开关
         * START_RECORD_TRACK 开始记录运行轨迹
         * STOP_RECORD_TRACK 停止记录运行轨迹
         * START_TRACK 开始运行轨迹
         * STOP_TRACK 停止运行轨迹；
         */
        private String command;
    }

    /**
     * 录像回放
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CameraBackPlay{
        /**
         *  监控点编号
         */
        private String cameraIndexCode;

        /**
         * ws
         */
        private String protocol;

        /**
         * 开始查询时间（IOS8601格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX）
         * 例如北京时间：
         * 2017-06-14T00:00:00.000+08:00，参考附录B ISO8601时间格式说明
         */
        private String beginTime;

        /**
         * 结束查询时间，开始时间和结束时间相差不超过三天；
         * （IOS8601格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX）例如北京时间：
         * 2017-06-15T00:00:00.000+08:00，参考附录B ISO8601时间格式说明
         */
        private String endTime;
    }


    /**
     * 抓图
     *
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CameraCatch{
        /**
         *  监控点编号
         */
        private String cameraIndexCode;

    }


    /**
     * 开始录制
     *
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CameraVideoStart{
        /**
         *  监控点编号
         */
        private String cameraIndexCode;

        /**
         * 6
         */
        private String recordType;

    }



    /**
     * 开始录制
     *
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CameraVideoStop{
        /**
         *  监控点编号
         */
        private String cameraIndexCode;

        /**
         * 任务ID
         */
        private String taskID;

    }


    @NoArgsConstructor
    @Data
    public static class AreaLinkVo{

        /**
         * 父区域编号
         */
        @JsonProperty("parentIndexCode")
        private String parentIndexCode;
        /**
         * 资源类型
         */
        @JsonProperty("resourceType")
        private String resourceType;
        /**
         * 页码
         */
        @JsonProperty("pageNo")
        private Integer pageNo;
        /**
         * 条数
         */
        @JsonProperty("pageSize")
        private Integer pageSize;
        /**
         * 级联标识
         */
        @JsonProperty("cascadeFlag")
        private Integer cascadeFlag;
    }
}
