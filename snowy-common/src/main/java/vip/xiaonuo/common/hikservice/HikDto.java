package vip.xiaonuo.common.hikservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/1 16:49
 */
public class HikDto {

    /**
     * 海康区域信息
     *
     * @author 张奇
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class HikAreaDto {
        /**
         * 区域唯一标识码
         */
        private String indexCode;
        /**
         * 区域名称
         */
        private String name;
        /**
         * 父区域唯一标识码
         */
        private String parentIndexCode;

        /**
         *  区域排序
         */
        private Long sort;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class HikAreaCameraListDto extends HikAreaDto{

        /**
         * 海康摄像头列表
         */
        private List<CameraDto> cameraList;

        /**
         * 子区域列表
         */
        private List<HikAreaCameraListDto> children;

    }


    /**
     *  海康区域信息分页
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class HikAreaPageDto{
        /**
         *  当前页码
         */
        private Integer pageNo;
        /**
         *  每页显示条数
         */
        private Integer pageSize;
        /**
         *   总条数
         */
        private Integer total;
        /**
         *    数据
         */
        private List<HikAreaDto> list;
    }

    /**
     *  海康摄像头信息分页
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class HikCameraPageDto{
        /**
         *  当前页码
         */
        private Integer pageNo;
        /**
         *  每页显示条数
         */
        private Integer pageSize;
        /**
         *   总条数
         */
        private Integer total;
        /**
         *    数据
         */
        private List<CameraDto> list;
    }


    /**
     *  海康摄像头在线信息分页
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class HikCameraLinePageDto{
        /**
         *  当前页码
         */
        private Integer pageNo;
        /**
         *  每页显示条数
         */
        private Integer pageSize;
        /**
         *   总条数
         */
        private Integer total;

        /**
         * 总页数
         */
        private Integer totalPage;
        /**
         *    数据
         */
        private List<CameraLineDto> list;
    }

    /**
     * 设备在线状态
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static  class CameraLineDto{

        @JsonProperty("deviceType")
        private String deviceType;
        @JsonProperty("regionIndexCode")
        private String regionIndexCode;
        @JsonProperty("collectTime")
        private String collectTime;
        @JsonProperty("deviceIndexCode")
        private String deviceIndexCode;
        @JsonProperty("port")
        private String port;
        @JsonProperty("ip")
        private String ip;
        @JsonProperty("regionName")
        private String regionName;
        @JsonProperty("indexCode")
        private String indexCode;
        @JsonProperty("online")
        private Integer online;
        @JsonProperty("cn")
        private String cn;
        @JsonProperty("treatyType")
        private String treatyType;
        @JsonProperty("manufacturer")
        private String manufacturer;
    }

    /**
     * 海康流地址
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class CameraUrlDto{
        /**
         * 地址
         */
        private String url;
    }

    /**
     * 摄像头
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class CameraDto{
        /**
         * 监控点编号
         */
        private String indexCode;

        /**
         * 所属区域
         */
        private String regionIndexCode;

        /**
         * 所属区域路径
         */
        private String regionPath;

        /**
         * 监控点国标编号
         */
        private String externalIndexCode;

        /**
         * 监控点名称
         */
        private String name;

        /**
         * 父级资源编号
         */
        private String parentIndexCode;

        /**
         * 经度
         */
        private String longitude;

        /**
         * 纬度
         */
        private String latitude;

        /**
         * 海拔高度
         */
        private String elevation;

        /**
         * 监控点类型 0枪机 1.半球 2.快球 3.云台枪机
         */
        private String cameraType;

        /**
         * 安装位置
         */
        private String installLocation;

        /**
         * 通道号
         */
        private Integer chanNum;

        /**
         * 级联编号
         */
        private String cascadeCode;

        /**
         * 所属DAC编号
         */
        private String dacIndexCode;

        /**
         * 设备能力集(含设备上的智能能力)
         */
        private String capability;

        /**
         * 配置录像后返回 回放时使用，中心存储：0，设备存储：1 ，多个录像存储位置以_分隔，示例：0_1
         */
        private String recordLocation;

        /**
         * 通道子类型 analog 模拟通道	 digital	数字通道 mirror	镜像通道 record	录播通道 zero	零通道
         */
        private String channelType;

        /**
         *  传输协议类型 1-TCP 0-UDP
         */
        private Integer transType;

        /**
         * 接入协议
         * 海康私有协议	hiksdk_net
         * 海康私有串口协议	hiksdk_com
         * eHome协议	ehome_reg
         * 大华私有协议	dhsdk_net
         * ONVIF协议	onvif_net
         * GB/T28181	gb_reg
         * 国网B接口协议	bi_reg
         * 汉军	hun_net
         * 高德威	gdw_net
         * 来邦	lbon_net
         * 益可达私有协议	yikedasdk_net
         * 瑞堡私有协议	hikrainbow_netcom
         * 依时利私有协议	eastriver_net
         * 海康地磁私有协议	hikgeo_reg
         * 海康地磁TCP协议	tcpgeo_reg
         * 海康地磁UDP协议	udpgeo_reg
         * 迈特安私有协议	mtasdk_reg
         * 海康LED私有协议	hikled_net
         * 海康LED私有串口协议	hikled_com
         * 诺瓦LED私有协议	novaled_net
         * 诺瓦LED私有串口协议	novaled_com
         */
        private String treatyType;

        /**
         * 创建时间
         */
        private String createTime;

        /**
         * 更新时间
         */
        private String updateTime;

        /**
         * rtsp流地址
         */
        private String rtspUrl;

        /**
         * ws流地址
         */
        private String wsUrl;
    }


    /**
     * 视频回放
     */
    @NoArgsConstructor
    @Data
    public static class CameraBackPlayDto{

        /**
         * 录像片段信息
         */
        @JsonProperty("list")
        private List<ListDTO> list;
        /**
         *分页标记
         */
        @JsonProperty("uuid")
        private String uuid;
        /**
         * 取流短url
         */
        @JsonProperty("url")
        private String url;

        @NoArgsConstructor
        @Data
        public static class ListDTO {
            /**
             * 查询录像的锁定类型，0-全部录像；1-未锁定录像；2-已锁定录像。
             */
            @JsonProperty("lockType")
            private Integer lockType;
            /**
             * 开始时间
             * 录像片段的开始时间（IOS8601格式yyyy-MM-dd’T’HH:mm:ss.SSSzzz
             */
            @JsonProperty("beginTime")
            private String beginTime;
            /**
             * 结束时间
             * 录像片段的结束时间（IOS8601格式yyyy-MM-dd’T’HH:mm:ss.SSSzzz
             */
            @JsonProperty("endTime")
            private String endTime;
            /**
             * 录像片段大小
             * 录像片段大小（单位：Byte）
             */
            @JsonProperty("size")
            private Integer size;
        }
    }

}
