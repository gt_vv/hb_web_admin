package vip.xiaonuo.common.hikservice;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/1 16:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HikResponseDto<T> {

    /**
     *  状态码
     */
    private String code;

    /**
     *  消息
     */
    private String message;

    /**
     *   数据
     */
    private T data;


    /**
     * 区域信息数据解析
     * @param jsonObject
     * @param <T>
     * @return
     */
    public static <T> HikResponseDto ofHikAreaPageList(JSONObject jsonObject) {
        return HikResponseDto.builder()
                .code(jsonObject.getStr("code"))
                .message(jsonObject.getStr("msg"))
                .data(JSONUtil.toBean(jsonObject.getStr("data"), HikDto.HikAreaPageDto.class))
                .build();
    }

    /**
     * 摄像头分页
     * @param jsonObject
     * @param <T>
     * @return
     */
    public static <T> HikResponseDto ofHikCameraPageList(JSONObject jsonObject) {
        return HikResponseDto.builder()
                .code(jsonObject.getStr("code"))
                .message(jsonObject.getStr("msg"))
                .data(JSONUtil.toBean(jsonObject.getStr("data"), HikDto.HikCameraPageDto.class))
                .build();
    }

    /**
     * 摄像头是否在线
     * @param jsonObject
     * @param <T>
     * @return
     */
    public static <T> HikResponseDto ofHikCameraLinePageList(JSONObject jsonObject) {
        return HikResponseDto.builder()
                .code(jsonObject.getStr("code"))
                .message(jsonObject.getStr("msg"))
                .data(JSONUtil.toBean(jsonObject.getStr("data"), HikDto.HikCameraLinePageDto.class))
                .build();
    }

    /**
     * 摄像头流地址
     * @param jsonObject
     * @param <T>
     * @return
     */
    public static <T> HikResponseDto ofHikCameraUrl(JSONObject jsonObject) {
        return HikResponseDto.builder()
                .code(jsonObject.getStr("code"))
                .message(jsonObject.getStr("msg"))
                .data(JSONUtil.toBean(jsonObject.getStr("data"), HikDto.CameraUrlDto.class))
                .build();
    }

    /**
     * 云台控制
     * @param jsonObject
     * @param <T>
     * @return
     */
    public static <T> HikResponseDto ofHikControl(JSONObject jsonObject) {
        return HikResponseDto.builder()
                .code(jsonObject.getStr("code"))
                .message(jsonObject.getStr("msg"))
                .data(jsonObject.getStr("code").equals("0") ? true : false)
                .build();
    }


    /**
     * 回放流地址
     * @param jsonObject
     * @param <T>
     * @return
     */
    public static <T> HikResponseDto ofHikBackPlay(JSONObject jsonObject) {
        return HikResponseDto.builder()
                .code(jsonObject.getStr("code"))
                .message(jsonObject.getStr("msg"))
                .data(JSONUtil.toBean(jsonObject.getStr("data"), HikDto.CameraBackPlayDto.class))
                .build();
    }

    /**
     * 图片地址
     * @param jsonObject
     * @param <T>
     * @return
     */
    public static <T> HikResponseDto ofHikPic(JSONObject jsonObject) {
        return HikResponseDto.builder()
                .code(jsonObject.getStr("code"))
                .message(jsonObject.getStr("msg"))
                .data(JSONUtil.parseObj(jsonObject.getStr("data")).getStr("picUrl"))
                .build();
    }

    /**
     * 录像任务ID
     * @param jsonObject
     * @param <T>
     * @return
     */
    public static <T> HikResponseDto ofHikVideo(JSONObject jsonObject) {
        return HikResponseDto.builder()
                .code(jsonObject.getStr("code"))
                .message(jsonObject.getStr("msg"))
                .data(JSONUtil.parseObj(jsonObject.getStr("data")).getStr("taskID"))
                .build();
    }


}
