package vip.xiaonuo.common.hikservice;

import cn.hutool.core.map.MapUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/1 16:15
 */
public enum HikUrlEnum {

    /**
     * 分页获取区域列表
     */
    AREA_LIST("/artemis/api/resource/v1/regions", "POST"),
    /**
     *  分页获取子区域列表
     */
    CHILD_AREA_LIST("/artemis/api/resource/v2/regions/subRegions", "POST"),

    /**
     * 资源列表
     */
    RESOURCE_LIST("/artemis/api/irds/v2/resource/resourcesByParams", "POST"),
    /**
     * 资源是否在线
     */
    RESOURCE_ONLINE("/artemis/api/nms/v1/online/camera/get", "POST"),
    /**
     *  云台控制
     */
    CAMERA_CONTROL("/artemis/api/video/v1/ptzs/controlling", "POST"),
    /**
     * 回放
     */
    CAMERA_BACK_PLAY("/artemis/api/video/v2/cameras/playbackURLs", "POST"),
    /**
     * 抓图
     */
    CAMERA_CATCH("/artemis/api/video/v1/manualCapture", "POST"),
    /**
     * 开始录制
     */
    CAMERA_VIDEO_START("/artemis/api/video/v1/manualRecord/start", "POST"),
    /**
     * 结束录制
     */
    CAMERA_VIDEO_END("/artemis/api/video/v1/manualRecord/stop", "POST"),
    /**
     * 流地址
     */
    RESOURCE_URL("/artemis/api/video/v2/cameras/previewURLs", "POST");

    /**
     * 请求API地址
     */
    private String url;

    /**
     * 请求方法 [POST,GET,PUT]
     */
    private String method;


    HikUrlEnum(String url, String method) {
        this.url = url;
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public String getMethod() {
        return method;
    }


    public Map<String, String> getMapUrl(){
        return MapUtil.builder(new HashMap<String,String>())
                .put("https://", this.url)
                .build();
    }


}
