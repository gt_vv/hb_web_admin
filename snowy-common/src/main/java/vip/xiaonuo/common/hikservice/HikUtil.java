package vip.xiaonuo.common.hikservice;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.hikvision.artemis.sdk.ArtemisHttpUtil;
import com.hikvision.artemis.sdk.config.ArtemisConfig;
import org.springframework.stereotype.Component;

/**
 * 海康安防工具类
 *
 * @author 张奇
 * @desc
 * @time 2024/3/1 15:55
 */
@Component
public class HikUtil {

    /**
     * 发送信息
     * @param hikUrlEnum
     * @param body
     * @return
     */
    public JSONObject sendMsg(HikUrlEnum hikUrlEnum, String body){
        String result = "";
        try {
             result = ArtemisHttpUtil.doPostStringArtemis(getConfig(), hikUrlEnum.getMapUrl(), body, null, null, "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONUtil.parseObj(result);
    }


    /**
     * 获取海康配置
     * @return
     */
    public ArtemisConfig getConfig() {
        ArtemisConfig config = new ArtemisConfig();
        config.setHost("10.153.13.228:443"); // 代理API网关nginx服务器ip端口
        config.setAppKey("23266557");  // 秘钥appkey
        config.setAppSecret("7TSXGBbfm3wromGVCOqc");// 秘钥appSecret
        return config;
    }


}
