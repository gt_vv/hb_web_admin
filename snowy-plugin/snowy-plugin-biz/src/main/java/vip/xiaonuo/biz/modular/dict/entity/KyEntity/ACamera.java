package vip.xiaonuo.biz.modular.dict.entity.KyEntity;

import lombok.Data;

@Data
public class ACamera {


    /**
     * 点位ID
     */
    private Integer id;


    /**
     * 点位indexCode
     */
    private String name;


    /**
     *  在线状态
     */
    private Integer status;


    /**
     * 流地址
     */
    private String stream;


}
