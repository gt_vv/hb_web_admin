/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.dict.entity.HbAlarm;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmAddParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmEditParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmIdParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmPageParam;
import vip.xiaonuo.biz.modular.dict.service.HbAlarmService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * hb_alarm控制器
 *
 * @author gt
 * @date  2024/03/04 15:08
 */
@Api(tags = "hb_alarm控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class HbAlarmController {

    @Resource
    private HbAlarmService hbAlarmService;

    /**
     * 获取hb_alarm分页
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取hb_alarm分页")
    @SaCheckPermission("/biz/alarm/page")
    @GetMapping("/biz/alarm/page")
    public CommonResult<Page<HbAlarm>> page(HbAlarmPageParam hbAlarmPageParam) {
        return CommonResult.data(hbAlarmService.page(hbAlarmPageParam));
    }

    /**
     * 添加hb_alarm
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加hb_alarm")
    @CommonLog("添加hb_alarm")
    @SaCheckPermission("/biz/alarm/add")
    @PostMapping("/biz/alarm/add")
    public CommonResult<String> add(@RequestBody @Valid HbAlarmAddParam hbAlarmAddParam) {
        hbAlarmService.add(hbAlarmAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑hb_alarm
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑hb_alarm")
    @CommonLog("编辑hb_alarm")
    @SaCheckPermission("/biz/alarm/edit")
    @PostMapping("/biz/alarm/edit")
    public CommonResult<String> edit(@RequestBody @Valid HbAlarmEditParam hbAlarmEditParam) {
        hbAlarmService.edit(hbAlarmEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除hb_alarm
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除hb_alarm")
    @CommonLog("删除hb_alarm")
    @SaCheckPermission("/biz/alarm/delete")
    @PostMapping("/biz/alarm/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<HbAlarmIdParam> hbAlarmIdParamList) {
        hbAlarmService.delete(hbAlarmIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取hb_alarm详情
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取hb_alarm详情")
    @SaCheckPermission("/biz/alarm/detail")
    @GetMapping("/biz/alarm/detail")
    public CommonResult<HbAlarm> detail(@Valid HbAlarmIdParam hbAlarmIdParam) {
        return CommonResult.data(hbAlarmService.detail(hbAlarmIdParam));
    }
}
