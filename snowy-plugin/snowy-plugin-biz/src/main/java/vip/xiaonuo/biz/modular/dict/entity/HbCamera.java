package vip.xiaonuo.biz.modular.dict.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;

/**
 * @desc 
 * @author 张奇
 * @time  2024/3/7 16:33
 */

/**
 * 摄像头
 */
@TableName(value = "hb_camera")
@Builder
public class HbCamera {
    /**
     * 摄像头编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 安防摄像头编号
     */
    @TableField(value = "index_code")
    private String indexCode;

    /**
     * 摄像头名称
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * rtsp地址
     */
    @TableField(value = "rtsp_url")
    private String rtspUrl;

    /**
     * ws地址
     */
    @TableField(value = "ws_url")
    private String wsUrl;

    /**
     * 摄像头类型 0枪机 1.半球 2.快球 3.云台枪机
     */
    @TableField(value = "camera_type")
    private String cameraType;

    /**
     * 区域编号
     */
    @TableField(value = "area_code")
    private String areaCode;

    /**
     * 区域名称
     */
    @TableField(value = "area_name")
    private String areaName;

    /**
     * 父级区域编号
     */
    @TableField(value = "parent_code")
    private String parentCode;

    /**
     * 父级区域名称
     */
    @TableField(value = "parent_name")
    private String parentName;

    /**
     * 获取摄像头编号
     *
     * @return id - 摄像头编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置摄像头编号
     *
     * @param id 摄像头编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取安防摄像头编号
     *
     * @return index_code - 安防摄像头编号
     */
    public String getIndexCode() {
        return indexCode;
    }

    /**
     * 设置安防摄像头编号
     *
     * @param indexCode 安防摄像头编号
     */
    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    /**
     * 获取摄像头名称
     *
     * @return name - 摄像头名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置摄像头名称
     *
     * @param name 摄像头名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取rtsp地址
     *
     * @return rtsp_url - rtsp地址
     */
    public String getRtspUrl() {
        return rtspUrl;
    }

    /**
     * 设置rtsp地址
     *
     * @param rtspUrl rtsp地址
     */
    public void setRtspUrl(String rtspUrl) {
        this.rtspUrl = rtspUrl;
    }

    /**
     * 获取ws地址
     *
     * @return ws_url - ws地址
     */
    public String getWsUrl() {
        return wsUrl;
    }

    /**
     * 设置ws地址
     *
     * @param wsUrl ws地址
     */
    public void setWsUrl(String wsUrl) {
        this.wsUrl = wsUrl;
    }

    /**
     * 获取摄像头类型 0枪机 1.半球 2.快球 3.云台枪机
     *
     * @return camera_type - 摄像头类型 0枪机 1.半球 2.快球 3.云台枪机
     */
    public String getCameraType() {
        return cameraType;
    }

    /**
     * 设置摄像头类型 0枪机 1.半球 2.快球 3.云台枪机
     *
     * @param cameraType 摄像头类型 0枪机 1.半球 2.快球 3.云台枪机
     */
    public void setCameraType(String cameraType) {
        this.cameraType = cameraType;
    }

    /**
     * 获取区域编号
     *
     * @return area_code - 区域编号
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 设置区域编号
     *
     * @param areaCode 区域编号
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * 获取区域名称
     *
     * @return area_name - 区域名称
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * 设置区域名称
     *
     * @param areaName 区域名称
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     * 获取父级区域编号
     *
     * @return parent_code - 父级区域编号
     */
    public String getParentCode() {
        return parentCode;
    }

    /**
     * 设置父级区域编号
     *
     * @param parentCode 父级区域编号
     */
    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    /**
     * 获取父级区域名称
     *
     * @return parent_name - 父级区域名称
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * 设置父级区域名称
     *
     * @param parentName 父级区域名称
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
}