/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.dict.entity.HbAlarm;
import vip.xiaonuo.biz.modular.dict.mapper.HbAlarmMapper;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmAddParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmEditParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmIdParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmPageParam;
import vip.xiaonuo.biz.modular.dict.service.HbAlarmService;

import java.util.List;

/**
 * hb_alarmService接口实现类
 *
 * @author gt
 * @date  2024/03/04 15:08
 **/
@Service
public class HbAlarmServiceImpl extends ServiceImpl<HbAlarmMapper, HbAlarm> implements HbAlarmService {

    @Override
    public Page<HbAlarm> page(HbAlarmPageParam hbAlarmPageParam) {
        QueryWrapper<HbAlarm> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(hbAlarmPageParam.getCameraName())) {
            queryWrapper.lambda().like(HbAlarm::getCameraName, hbAlarmPageParam.getCameraName());
        }
        if(ObjectUtil.isNotEmpty(hbAlarmPageParam.getStartAlarmTime()) && ObjectUtil.isNotEmpty(hbAlarmPageParam.getEndAlarmTime())) {
            queryWrapper.lambda().between(HbAlarm::getAlarmTime, hbAlarmPageParam.getStartAlarmTime(), hbAlarmPageParam.getEndAlarmTime());
        }
        if(ObjectUtil.isNotEmpty(hbAlarmPageParam.getAlarmType())) {
            queryWrapper.lambda().like(HbAlarm::getAlarmType, hbAlarmPageParam.getAlarmType());
        }
        if(ObjectUtil.isNotEmpty(hbAlarmPageParam.getAlarmLevel())) {
            queryWrapper.lambda().like(HbAlarm::getAlarmLevel, hbAlarmPageParam.getAlarmLevel());
        }
        if(ObjectUtil.isAllNotEmpty(hbAlarmPageParam.getSortField(), hbAlarmPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(hbAlarmPageParam.getSortOrder());
            queryWrapper.orderBy(true, hbAlarmPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(hbAlarmPageParam.getSortField()));
        } else {
            //queryWrapper.lambda().orderByAsc(HbAlarm::getId);
        }
        queryWrapper.lambda().orderByDesc(HbAlarm :: getId);
        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(HbAlarmAddParam hbAlarmAddParam) {
        HbAlarm hbAlarm = BeanUtil.toBean(hbAlarmAddParam, HbAlarm.class);
        this.save(hbAlarm);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(HbAlarmEditParam hbAlarmEditParam) {
        HbAlarm hbAlarm = this.queryEntity(String.valueOf(hbAlarmEditParam.getId()));
        BeanUtil.copyProperties(hbAlarmEditParam, hbAlarm);
        this.updateById(hbAlarm);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<HbAlarmIdParam> hbAlarmIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(hbAlarmIdParamList, HbAlarmIdParam::getId));
    }

    @Override
    public HbAlarm detail(HbAlarmIdParam hbAlarmIdParam) {
        return this.queryEntity(String.valueOf(hbAlarmIdParam.getId()));
    }

    @Override
    public HbAlarm queryEntity(String id) {
        HbAlarm hbAlarm = this.getById(id);
        if(ObjectUtil.isEmpty(hbAlarm)) {
            throw new CommonException("hb_alarm不存在，id值为：{}", id);
        }
        return hbAlarm;
    }

    @Override
    public List<JSONObject> alarmStatistics() {
        return  this.baseMapper.alarmStatisticsMapper();

    }

    @Override
    public Long alarmStatisticsByMonth(String monthStartTimeStr, String monthEndTimeStr) {
        return null;
    }

    @Override
    public Long alarmStatisticsByTime(String monthStartTimeStr, String monthEndTimeStr) {
        return null;
    }
}
