/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.common.enums.CommonSortOrderEnum;
import vip.xiaonuo.common.exception.CommonException;
import vip.xiaonuo.common.kyservice.Kyhttp;
import vip.xiaonuo.common.page.CommonPageRequest;
import vip.xiaonuo.biz.modular.dict.entity.HbAlg;
import vip.xiaonuo.biz.modular.dict.mapper.HbAlgMapper;
import vip.xiaonuo.biz.modular.dict.param.HbAlgAddParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgEditParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgIdParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgPageParam;
import vip.xiaonuo.biz.modular.dict.service.HbAlgService;

import javax.annotation.Resource;
import java.util.List;

/**
 * hb_algService接口实现类
 *
 * @author 11
 * @date  2024/03/04 15:21
 **/
@Service
public class HbAlgServiceImpl extends ServiceImpl<HbAlgMapper, HbAlg> implements HbAlgService {

    @Resource
    private Kyhttp kyhttp;

    @Override
    public Page<HbAlg> page(HbAlgPageParam hbAlgPageParam) {
        //保持同步 ky  与数据库
        JSONObject alg = kyhttp.getAlg();
        //ky算法列表
        JSONArray algArr = alg.getJSONObject("data").getJSONArray("algoConfig");
        for (Object o : algArr) {
            JSONObject algJson = JSONUtil.parseObj(o);
            HbAlg hbAlg = new HbAlg();
            hbAlg.setAlgCode(algJson.getStr("task_value"));
            hbAlg.setAlgName(algJson.getStr("name"));
            hbAlg.setId(algJson.getInt("task_key"));
            HbAlg byId = this.getById(algJson.getInt("task_key"));
            if(ObjectUtil.isNotEmpty(byId)){
                hbAlg.setAlgLevel(byId.getAlgLevel());
                hbAlg.setAlgOthername(byId.getAlgOthername());
                this.updateById(hbAlg);
            }else {
                this.save(hbAlg);
            }

        }


        QueryWrapper<HbAlg> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(hbAlgPageParam.getAlgName())) {
            queryWrapper.lambda().like(HbAlg::getAlgName, hbAlgPageParam.getAlgName());
        }
        if(ObjectUtil.isNotEmpty(hbAlgPageParam.getAlgLevel())) {
            queryWrapper.lambda().like(HbAlg::getAlgLevel, hbAlgPageParam.getAlgLevel());
        }
        if(ObjectUtil.isAllNotEmpty(hbAlgPageParam.getSortField(), hbAlgPageParam.getSortOrder())) {
            CommonSortOrderEnum.validate(hbAlgPageParam.getSortOrder());
            queryWrapper.orderBy(true, hbAlgPageParam.getSortOrder().equals(CommonSortOrderEnum.ASC.getValue()),
                    StrUtil.toUnderlineCase(hbAlgPageParam.getSortField()));
        } else {
            queryWrapper.lambda().orderByAsc(HbAlg::getId);
        }

        return this.page(CommonPageRequest.defaultPage(), queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(HbAlgAddParam hbAlgAddParam) {
        HbAlg hbAlg = BeanUtil.toBean(hbAlgAddParam, HbAlg.class);
        this.save(hbAlg);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(HbAlgEditParam hbAlgEditParam) {
        HbAlg hbAlg = this.queryEntity(String.valueOf(hbAlgEditParam.getId()));
        BeanUtil.copyProperties(hbAlgEditParam, hbAlg);
        this.updateById(hbAlg);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<HbAlgIdParam> hbAlgIdParamList) {
        // 执行删除
        this.removeByIds(CollStreamUtil.toList(hbAlgIdParamList, HbAlgIdParam::getId));
    }

    @Override
    public HbAlg detail(HbAlgIdParam hbAlgIdParam) {
        return this.queryEntity(String.valueOf(hbAlgIdParam.getId()));
    }

    @Override
    public HbAlg queryEntity(String id) {
        HbAlg hbAlg = this.getById(id);
        if(ObjectUtil.isEmpty(hbAlg)) {
            throw new CommonException("hb_alg不存在，id值为：{}", id);
        }
        return hbAlg;
    }

    @Override
    public Object getList() {
        return this.list();
    }
}
