package vip.xiaonuo.biz.modular.dict.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import vip.xiaonuo.biz.modular.dict.entity.HbAlg;
import vip.xiaonuo.biz.modular.dict.entity.HbAlgteam;
import vip.xiaonuo.biz.modular.dict.entity.HbCamera;
import vip.xiaonuo.biz.modular.dict.entity.KyEntity.*;
import vip.xiaonuo.biz.modular.dict.entity.dto.HbTask;
import vip.xiaonuo.biz.modular.dict.entity.dto.KyCamera;
import vip.xiaonuo.biz.modular.dict.mapper.HbCameraMapper;
import vip.xiaonuo.biz.modular.dict.service.HbAlgService;
import vip.xiaonuo.biz.modular.dict.service.HbAlgteamService;
import vip.xiaonuo.biz.modular.dict.service.IHikService;
import vip.xiaonuo.biz.modular.dict.service.KyService;
import vip.xiaonuo.common.hikservice.HikDto;
import vip.xiaonuo.common.hikservice.HikVo;
import vip.xiaonuo.common.kyservice.Kyhttp;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class KyServiceImpl implements KyService {

    @Resource
    private Kyhttp kyhttp;

    @Resource
    private HbAlgteamService hbAlgteamService;


    @Resource
    private HbAlgService hbAlgService;

    @Resource
    private HbCameraMapper hbCameraMapper;

    @Resource
    private IHikService iHikService;


    @Override
    public long residueAlg() {
        //获取所有的任务
        long count = 0;
        JSONArray taskList = kyhttp.getTaskList();
        for (Object o : taskList) {
            ATask aTask = JSONUtil.toBean(JSONUtil.parseObj(o), ATask.class);
            //获取任务详情
            JSONArray taskDetail = kyhttp.getTaskDetail(aTask.getId());
            for (Object taskde : taskDetail) {
                System.out.println(taskde);
                ATaskDetail aTaskDetail = JSONUtil.toBean(JSONUtil.parseObj(taskde), ATaskDetail.class);
                List<ATaskDetailConfig> configList = aTaskDetail.getConfigList();
                List<String> cameraIdList = new ArrayList<>();
                if (ObjectUtil.isNotEmpty(configList)) {
                    long count1 = configList.stream().map(ATaskDetailConfig::getCameraId).count();
                    count += count1;
                }
            }
        }
        return 192 - count;
    }

    @Override
    public CommonResult starTtask(KyCamera kyCamera) {
        //获取算法 名称与ID的对应关系
        JSONObject alg = kyhttp.getAlg();
        JSONArray jsonArray = alg.getJSONObject("data").getJSONArray("algoConfig");
        Map<String, String> nameToAlgId = new HashMap<>();

        Map<Integer, String> idToName = new HashMap<>();
        Map<String, Integer> nameToId = new HashMap<>();
        List<String> algIdList = new ArrayList<>();
        for (Object o : jsonArray) {
            JSONObject algOne = JSONUtil.parseObj(o);
            nameToAlgId.put(algOne.getStr("name"), String.valueOf(algOne.getInt("task_key")));
            idToName.put(algOne.getInt("task_key"), algOne.getStr("name"));
            algIdList.add(algOne.getStr("task_key"));
            nameToId.put(algOne.getStr("name"), algOne.getInt("task_key"));
        }

        //传入的算法名称set
        Set<String> algSet = new HashSet<>();
        //传入的算法IDset
        Set<String> algIdSet = new HashSet<>();
        if (kyCamera.getType() == 1) {
            //0 算法列表
            //获取算法组内相关算法 ID
            QueryWrapper<HbAlgteam> qw = new QueryWrapper<>();
            qw.lambda().in(HbAlgteam::getId, kyCamera.getTeamIds());
            List<HbAlgteam> list = hbAlgteamService.list(qw);
            //List<String> algList = new ArrayList<>();

            for (HbAlgteam hbAlgteam : list) {
                if (hbAlgteam.getPId() == 0) {
                    //父级 pid 为 0
                    QueryWrapper<HbAlgteam> qw1 = new QueryWrapper<>();
                    qw1.lambda().eq(HbAlgteam::getPId, hbAlgteam.getId());
                    List<HbAlgteam> list1 = hbAlgteamService.list(qw1);
                    for (HbAlgteam algteam : list1) {
                        algSet.add(idToName.get(algteam.getAlgKeyId()));
                        algIdSet.add(String.valueOf(algteam.getAlgKeyId()));
                    }
                } else {
                    //具体算法
                    algSet.add(idToName.get(hbAlgteam.getAlgKeyId()));
                    algIdSet.add(String.valueOf(hbAlgteam.getAlgKeyId()));
                }

            }
        } else if (kyCamera.getType() == 0) {
            //算法组
            for (Integer teamId : kyCamera.getTeamIds()) {
                algSet.add(idToName.get(teamId));
                algIdSet.add(String.valueOf(teamId));
            }

        }

        long residueAlg = residueAlg();
        System.out.println("剩余算力" + residueAlg);
        List<Integer> aboutCarameAlg = getAboutCarameAlg(kyCamera.getIndexCode());
        if (aboutCarameAlg != null && aboutCarameAlg.size() != 0) {
            Set<String> idSet = new HashSet<>();
            idSet.addAll(algIdSet);
            for (Integer i : aboutCarameAlg) {
                idSet.add(String.valueOf(i));
            }

            if (idSet.size() > aboutCarameAlg.size()) {
                System.out.println("------------------------");
                idSet.forEach(System.out::println);
                algIdSet.forEach(System.out::println);
                aboutCarameAlg.forEach(System.out::println);
                System.out.println("应当运行的数量" + idSet.size());
                System.out.println("当前已有数量" + aboutCarameAlg.size());
                if ((idSet.size() - aboutCarameAlg.size()) > residueAlg) {
                    return CommonResult.error("算力不足");
                }
            }

        } else {
            if (Long.valueOf(algSet.size()) > residueAlg) {
                //下发的算法数量大于 剩余算力则返回失败
                return CommonResult.error("算力不足");
            }
        }

        try{
            HikVo.CameraUrlVo cameraUrlVo = new HikVo.CameraUrlVo();
            cameraUrlVo.setCameraIndexCode(kyCamera.getIndexCode());
            HikDto.CameraUrlDto cameraUrl = iHikService.getCameraUrl(cameraUrlVo);
            String url = cameraUrl.getUrl();
            kyCamera.setStream(url);
            //kyCamera.setStream("rtsp://admin:qwert12345@192.168.4.11:554/h264/ch1/main/av_stream");
        }catch (Exception e){
            kyCamera.setStream(String.valueOf(System.currentTimeMillis()));
        }

        //查询ky摄像头
        //System.out.println(kyCamera);
        JSONArray cameraByKey = kyhttp.getCameraByKey(kyCamera.getIndexCode());
        ACamera aCamera = new ACamera();
        try {
            aCamera = JSONUtil.toBean(JSONUtil.parseObj(cameraByKey.get(0)), ACamera.class);
        } catch (Exception e) {
            //添加摄像头
            String res = kyhttp.addCamera(kyCamera.getIndexCode(), kyCamera.getStream());
            if (StrUtil.isBlankIfStr(res)) {
                JSONArray camera = kyhttp.getCameraByKey(kyCamera.getIndexCode());
                aCamera = JSONUtil.toBean(JSONUtil.parseObj(camera.get(0)), ACamera.class);
            } else {
                return CommonResult.error(res);
            }
        }


        //查看是否存在任务
        JSONArray taskList = kyhttp.getTaskList();
        System.out.println(taskList);
        ATask aTask = new ATask();
        boolean taskFlag = true;
        boolean taskisOk = false;
        if (ObjectUtil.isNotEmpty(taskList)) {
            for (Object o : taskList) {
                aTask = JSONUtil.toBean(JSONUtil.parseObj(o), ATask.class);
                if (aTask.getName().equals("快速配置")) {
                    //存在任务且要先停止
                    taskisOk = true;
                    kyhttp.stopTask(aTask.getId());
                }
            }
        }
        if (taskisOk) {
            //创建任务
            aTask.setName("快速配置");
            Integer taskId = kyhttp.addTask(aTask.getName());
            aTask.setId(taskId);
            //给任务关联报警推送地址
            JSONObject callbackList = kyhttp.getCallbackList();
            JSONArray jsonArray1 = callbackList.getJSONArray("callbackList");
            KyCallBack callBack = JSONUtil.toBean(JSONUtil.parseObj(jsonArray1.get(0)), KyCallBack.class);
            //Integer taskId,Integer callbackId,Integer interval,Integer content)
            kyhttp.editCallback(taskId,callBack.getId(),20,4);
            taskFlag = false;
        }


        //向任务编辑所有算法


        //查看算法详情
        JSONArray taskDetail = kyhttp.getTaskDetail(aTask.getId());
        List<ATaskDetail> taskListEntity = new ArrayList<>();
        for (Object o : taskDetail) {
            taskListEntity.add(JSONUtil.toBean(JSONUtil.parseObj(o), ATaskDetail.class));
        }
        //归纳收集 当前摄像头 正在执行的算法
        List<String> algListNow = new ArrayList<>();
        Set<String> algIdSetNow = new HashSet<>();
        //当前算法所包含的摄像头
        Map<String, List<String>> algAndCamera = new HashMap<>();
        for (ATaskDetail aTaskDetail : taskListEntity) {
            System.out.println("任务详情" + aTaskDetail);
            List<ATaskDetailConfig> configList = aTaskDetail.getConfigList();
            List<String> cameraList = new ArrayList<>();

            if (aTaskDetail.getConfigList().size() != 0) {
                algListNow.add(aTaskDetail.getName());
            }
            algIdSetNow.add(nameToAlgId.get(aTaskDetail.getName()));
            for (ATaskDetailConfig aTaskDetailConfig : configList) {
                if (aTaskDetailConfig.getCameraId().equals(aCamera.getId())) {
                    cameraList.add(String.valueOf(aTaskDetailConfig.getCameraId()));
                }
            }
            algAndCamera.put(aTaskDetail.getName(), cameraList);
        }
        algIdSetNow.addAll(algIdSet);


        //向任务编辑算法
        kyhttp.taskAlg(aTask.getId(), algIdSetNow);


        if (algListNow == null || algListNow.size() == 0) {
            taskFlag = false;
        } else {
            taskFlag = true;
        }

        if (taskFlag) {
            for (String newAlg : algSet) {
                try {
                    List<String> cameraList = algAndCamera.get(newAlg);
                    cameraList.add(String.valueOf(aCamera.getId()));
                    kyhttp.taskAlgAndCamare(aTask.getId(), String.valueOf(nameToAlgId.get(newAlg)), cameraList);
                } catch (Exception e) {
                    List<String> s = new ArrayList<>();
                    s.add(String.valueOf(aCamera.getId()));
                    kyhttp.taskAlgAndCamare(aTask.getId(), String.valueOf(nameToAlgId.get(newAlg)), s);
                }
            }
            if (kyCamera.getType() == 0) {
                //删除 当前执行的 在新配置中未存在的算法  algListNow 当前正在执行的算法  algSet 新设置的算法
                for (String algnow : algListNow) {
                    //新设置的 中如果不包含 则要进行删除
                    if (!algSet.contains(algnow)) {
                        List<String> cameraList = algAndCamera.get(algnow);
                        if (ObjectUtil.isNotEmpty(cameraList)) {
                            cameraList.remove(String.valueOf(aCamera.getId()));
                            kyhttp.taskAlgAndCamare(aTask.getId(), String.valueOf(nameToAlgId.get(algnow)), cameraList);
                        }
                    }
                }
            }
        } else {
            List<String> cameraList = new ArrayList<>();
            cameraList.add(String.valueOf(aCamera.getId()));
            //直接添加或者关联算法
            for (String newAlg : algSet) {
                kyhttp.taskAlgAndCamare(aTask.getId(), String.valueOf(nameToAlgId.get(newAlg)), cameraList);
            }
        }
        ATask finalATask = aTask;
        new Thread(() -> {
            kyhttp.startTask(finalATask.getId());
        }).start();

        return CommonResult.ok("任务下发成功，已启动运行");
    }

    @Override
    public List<Integer> getAboutCarameAlg(String indexCode) {

        List<HbAlg> list = hbAlgService.list();
        Map<String, Integer> nameToIdMap = list.stream().collect(Collectors.toMap(HbAlg::getAlgName, HbAlg::getId));
        //List<String> algLsit = new ArrayList<>();
        List<Integer> algIdLsit = new ArrayList<>();
        //获取所有的任务
        JSONArray taskList = kyhttp.getTaskList();
        for (Object o : taskList) {
            ATask aTask = JSONUtil.toBean(JSONUtil.parseObj(o), ATask.class);
            if (!aTask.getName().equals("快速配置")) {
                continue;
            }
            //获取任务详情
            JSONArray taskDetail = kyhttp.getTaskDetail(aTask.getId());
            System.out.println(taskDetail);
            //List<ATaskDetail> taskListEntity = new ArrayList<>();
            for (Object taskde : taskDetail) {
                ATaskDetail aTaskDetail = JSONUtil.toBean(JSONUtil.parseObj(taskde), ATaskDetail.class);
                //taskListEntity.add(JSONUtil.toBean(JSONUtil.parseObj(taskde),ATaskDetail.class));
                String algName = aTaskDetail.getName();
                List<ATaskDetailConfig> configList = aTaskDetail.getConfigList();
                if (ObjectUtil.isNotEmpty(configList)) {
                    List<String> cameraIndexCodeList = configList.stream().map(ATaskDetailConfig::getName).collect(Collectors.toList());
                    if (cameraIndexCodeList.contains(indexCode)) {
                        //algLsit.add(algName);
                        Integer algId = nameToIdMap.get(algName);
                        algIdLsit.add(algId);
                    }
                }
            }
        }
        return algIdLsit;
    }

    @Override
    public Page<AKyTask> getTaskList(String keywords, Integer current, Integer size) {

        //获取算法 名称与ID的对应关系
        JSONObject alg = kyhttp.getAlg();
        JSONArray jsonArray = alg.getJSONObject("data").getJSONArray("algoConfig");
        Map<String, String> nameToAlgId = new HashMap<>();

        Map<Integer, String> idToName = new HashMap<>();
        Map<String, Integer> nameToId = new HashMap<>();
        List<String> algIdList = new ArrayList<>();
        for (Object o : jsonArray) {
            JSONObject algOne = JSONUtil.parseObj(o);
            nameToAlgId.put(algOne.getStr("name"), String.valueOf(algOne.getInt("task_key")));
            idToName.put(algOne.getInt("task_key"), algOne.getStr("name"));
            algIdList.add(algOne.getStr("task_key"));
            nameToId.put(algOne.getStr("name"), algOne.getInt("task_key"));
        }


        //获取摄像头对应关系
        List<HbCamera> hbCameras = hbCameraMapper.selectList(null);
        Map<String, String> indexCOdeToName = hbCameras.stream().collect(Collectors.toMap(HbCamera::getIndexCode, HbCamera::getName));

        JSONArray taskList = kyhttp.getTaskList();
        List<AKyTask> aKyTasks = new ArrayList<>();
        for (Object o : taskList) {
            AKyTask kyTask = JSONUtil.toBean(JSONUtil.parseObj(o), AKyTask.class);
            //查看任务详情
            JSONArray taskDetail = kyhttp.getTaskDetail(kyTask.getId());
            Set<String> algSet = new HashSet<>();
            Set<String> cameraSet = new HashSet<>();
            Set<String> indexCodeSet = new HashSet<>();
            for (Object detail : taskDetail) {
                ATaskDetail aTaskDetail = JSONUtil.toBean(JSONUtil.parseObj(detail), ATaskDetail.class);
                algSet.add(aTaskDetail.getName());
                List<ATaskDetailConfig> configList = aTaskDetail.getConfigList();
                for (ATaskDetailConfig aTaskDetailConfig : configList) {
                    cameraSet.add(indexCOdeToName.get(aTaskDetailConfig.getName()));
                    indexCodeSet.add(aTaskDetailConfig.getName());
                }
            }
            kyTask.setAlg(algSet.toString().replaceAll("\\[", "").replaceAll("]", "").replaceAll("\"", ""));

            kyTask.setCamera(cameraSet.toString().replaceAll("\\[", "").replaceAll("]", "").replaceAll("\"", ""));
            List<String> indexCodeList = new ArrayList<>();
            for (String indexCode : indexCodeSet) {
                indexCodeList.add(indexCode);
            }
            List<Integer> algList = new ArrayList<>();
            for (String a : algSet) {
                algList.add(nameToId.get(a));
            }
            kyTask.setType(kyTask.getStatus());
            kyTask.setIndexCodeList(indexCodeList);
            kyTask.setAlgList(algList);
            aKyTasks.add(kyTask);

        }
        for (AKyTask aKyTask : aKyTasks) {
            System.out.println(JSONUtil.parseObj(aKyTask));
        }
        //搜索相关
        List<AKyTask> keyList = new ArrayList<>();
        if (StrUtil.isNotBlank(keywords)) {
            for (AKyTask aKyTask : aKyTasks) {
                if (aKyTask.getCamera().contains(keywords) || aKyTask.getAlg().contains(keywords) || aKyTask.getName().contains(keywords)) {
                    keyList.add(aKyTask);
                }
            }
        } else {
            keyList.addAll(aKyTasks);
        }
        // 快速配置 始终为第一项
        List<AKyTask> respList = new ArrayList<>();
        for (AKyTask kyTask : keyList) {
            if (kyTask.getName().equals("快速配置")) {
                kyTask.setCamera("/");
                kyTask.setAlg("/");
                respList.add(kyTask);
            }
        }
        for (AKyTask kyTask : keyList) {
            if (!kyTask.getName().equals("快速配置")) {
                respList.add(kyTask);
            }
        }


        //Page
        List<AKyTask> collect = respList.stream()
                .skip(size * (current - 1))
                .limit(size)
                .collect(Collectors.toList());
        Page<AKyTask> page = new Page<>();


        if (collect.size() < size) {
            page.setPages(1);
        } else {
            page.setPages((collect.size() / size) + 1);
        }


        page.setSize(size);
        page.setTotal(aKyTasks.size());
        page.setCurrent(current);
        page.setRecords(collect);
        return page;

    }

    @Override
    public boolean createTask(KyCreateTask kyCreateTask) {
        //获取摄像头列表
        JSONArray cameraByKey = kyhttp.getCamera();
        List<String> indexCodeList = new ArrayList<>();
        for (Object o : cameraByKey) {
            ACamera aCamera = JSONUtil.toBean(JSONUtil.parseObj(o), ACamera.class);
            System.out.println(aCamera);
            if (kyCreateTask.getIndexCodeList().contains(aCamera.getName())) {
                indexCodeList.add(aCamera.getName());
                //摄像头 已存在 算法端
                if (aCamera.getStatus() == 0) {
                    //摄像头不在线  更换流地址
                    try{
                        HikVo.CameraUrlVo cameraUrlVo = new HikVo.CameraUrlVo();
                        cameraUrlVo.setCameraIndexCode(aCamera.getName());
                        HikDto.CameraUrlDto cameraUrl = iHikService.getCameraUrl(cameraUrlVo);
                        String url = cameraUrl.getUrl();
                        kyhttp.editCamera(aCamera.getId(),aCamera.getName(),url);
                    }catch (Exception e){
                        String url = String.valueOf(System.currentTimeMillis());  //此处线上应进行注释
                        kyhttp.editCamera(aCamera.getId(),aCamera.getName(),url);
                    }


                    //刷新摄像头
                    kyhttp.cameraRefresh(aCamera.getId());
                }
            }else{

            }
        }

        for (String indexCOde : kyCreateTask.getIndexCodeList()) {
            if(!indexCodeList.contains(indexCOde)){
                //新增摄像头
                try {
                    HikVo.CameraUrlVo cameraUrlVo = new HikVo.CameraUrlVo();
                    cameraUrlVo.setCameraIndexCode(indexCOde);
                    HikDto.CameraUrlDto cameraUrl = iHikService.getCameraUrl(cameraUrlVo);
                    String url = cameraUrl.getUrl();
                    kyhttp.addCamera(indexCOde,url);
                }catch (Exception e) {
                    String url = String.valueOf(System.currentTimeMillis());  //此处线上应进行注释
                    kyhttp.addCamera(indexCOde,url);
                }

            }
        }

        //新增任务
        Integer taskId = kyhttp.addTask(kyCreateTask.getName());

        //给任务关联报警推送地址
        JSONObject callbackList = kyhttp.getCallbackList();
        JSONArray jsonArray1 = callbackList.getJSONArray("callbackList");
        KyCallBack callBack = JSONUtil.toBean(JSONUtil.parseObj(jsonArray1.get(0)), KyCallBack.class);
        //Integer taskId,Integer callbackId,Integer interval,Integer content)
        kyhttp.editCallback(taskId,callBack.getId(),20,4);

        Set<String> algList = new HashSet<>();
        for (String s : kyCreateTask.getAlgList()) {
            algList.add(s);
        }
        //给任务关联回调接口
        JSONArray all = kyhttp.getCamera();
        List<ACamera> cameras = new ArrayList<>();
        for (Object o : all) {
            ACamera aCamera = JSONUtil.toBean(JSONUtil.parseObj(o), ACamera.class);
            cameras.add(aCamera);
        }
        Map<String, Integer> nameToIdCameraMap = cameras.stream().collect(Collectors.toMap(ACamera::getName, ACamera::getId));
        List<String>  cameraIdList = new ArrayList<>();
        for (String s : kyCreateTask.getIndexCodeList()) {
            cameraIdList.add(String.valueOf(nameToIdCameraMap.get(s)));
        }

        kyhttp.taskAlg(taskId,algList);
        for (String algStr : algList) {
            kyhttp.taskAlgAndCamare(taskId,algStr,cameraIdList);
        }
        if(kyCreateTask.getType().equals("1")){
            kyhttp.startTask(taskId);
        }
        return true;
    }

    @Override
    public boolean editTask(KyCreateTask kyCreateTask) {
        //对任务进行停止操作
        kyhttp.stopTask(kyCreateTask.getId());
        //编辑任务名称
        kyhttp.editTask(kyCreateTask.getId(),kyCreateTask.getName());
        //查看任务详情
        //编辑任务算法
        Set<String> algList = new HashSet<>();
        for (String s : kyCreateTask.getAlgList()) {
            algList.add(s);
        }
        kyhttp.taskAlg(kyCreateTask.getId(),algList);


        //摄像头操作
        JSONArray cameraByKey = kyhttp.getCamera();
        List<String> indexCodeList = new ArrayList<>();
        for (Object o : cameraByKey) {
            ACamera aCamera = JSONUtil.toBean(JSONUtil.parseObj(o), ACamera.class);
            System.out.println(aCamera);
            if (kyCreateTask.getIndexCodeList().contains(aCamera.getName())) {
                indexCodeList.add(aCamera.getName());
                //摄像头 已存在 算法端
                if (aCamera.getStatus() == 0) {
                    //摄像头不在线  更换流地址
                    try{
                        HikVo.CameraUrlVo cameraUrlVo = new HikVo.CameraUrlVo();
                        cameraUrlVo.setCameraIndexCode(aCamera.getName());
                        HikDto.CameraUrlDto cameraUrl = iHikService.getCameraUrl(cameraUrlVo);
                        String url = cameraUrl.getUrl();
                        kyhttp.editCamera(aCamera.getId(),aCamera.getName(),url);
                    }catch (Exception e){
                        String url = String.valueOf(System.currentTimeMillis());  //此处线上应进行注释
                        kyhttp.editCamera(aCamera.getId(),aCamera.getName(),url);
                    }


                    //刷新摄像头
                    kyhttp.cameraRefresh(aCamera.getId());
                }
            }else{

            }
        }
        for (String indexCOde : kyCreateTask.getIndexCodeList()) {
            if(!indexCodeList.contains(indexCOde)){
                //新增摄像头
                try {
                    HikVo.CameraUrlVo cameraUrlVo = new HikVo.CameraUrlVo();
                    cameraUrlVo.setCameraIndexCode(indexCOde);
                    HikDto.CameraUrlDto cameraUrl = iHikService.getCameraUrl(cameraUrlVo);
                    String url = cameraUrl.getUrl();
                    kyhttp.addCamera(indexCOde,url);
                }catch (Exception e) {
                    String url = String.valueOf(System.currentTimeMillis());  //此处线上应进行注释
                    kyhttp.addCamera(indexCOde, url);
                }

            }
        }

        JSONArray all = kyhttp.getCamera();
        List<ACamera> cameras = new ArrayList<>();
        for (Object o : all) {
            ACamera aCamera = JSONUtil.toBean(JSONUtil.parseObj(o), ACamera.class);
            cameras.add(aCamera);
        }
        Map<String, Integer> nameToIdCameraMap = cameras.stream().collect(Collectors.toMap(ACamera::getName, ACamera::getId));
        List<String>  cameraIdList = new ArrayList<>();
        for (String s : kyCreateTask.getIndexCodeList()) {
            cameraIdList.add(String.valueOf(nameToIdCameraMap.get(s)));
        }
        for (String algStr : algList) {
            kyhttp.taskAlgAndCamare(kyCreateTask.getId(),algStr,cameraIdList);
        }
        //跟新算法与摄像头的绑定关系

        return false;
    }

    @Override
    public boolean taskDel(Integer id) {
        // 停止任务
        kyhttp.stopTask(id);
        // 删除任务
        kyhttp.taskDelete(id);
        return false;
    }

    /**
     * 快速配置
     * @param kyCamera
     * @return
     */
    @Override
    public CommonResult task(KyCamera kyCamera) {
        /**
         * 算法服务调用--
         */
        //获取任务 是否有快速配置的任务
        return null;
    }


}
