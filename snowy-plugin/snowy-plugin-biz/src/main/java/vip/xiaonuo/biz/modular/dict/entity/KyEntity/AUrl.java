package vip.xiaonuo.biz.modular.dict.entity.KyEntity;

import lombok.Data;

@Data
public class AUrl {

    private Integer algorithmId;

    private String algorithmName;

    private String cameraId;

    private String cameraName;
    private String key;
    private String url;
}
