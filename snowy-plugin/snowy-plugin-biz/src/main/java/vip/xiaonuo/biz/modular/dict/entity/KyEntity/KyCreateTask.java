package vip.xiaonuo.biz.modular.dict.entity.KyEntity;

import lombok.Data;

import java.util.List;

@Data
public class KyCreateTask {

    private Integer id;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 摄像头 IndexCode
     */
    private List<String> indexCodeList;

    /**
     * 算法列表
     */
    private List<String> algList;

    /**
     * 启用并创建
     */
    private String type;
}
