package vip.xiaonuo.biz.modular.dict.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @desc 
 * @author 张奇
 * @time  2024/3/7 16:33
 */

/**
 * 播放组
 */
@TableName(value = "hb_play_group")
public class HbPlayGroup {
    /**
     * 组编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 组名称
     */
    @TableField(value = "group_name")
    private String groupName;

    /**
     * 父级组编号
     */
    @TableField(value = "parent_id")
    private Long parentId;

    /**
     * 0.根 1.一级 2. 二级 3.三级 ...
     */
    @TableField(value = "group_leve")
    private Long groupLeve;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Long createTime;

    /**
     * 获取组编号
     *
     * @return id - 组编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置组编号
     *
     * @param id 组编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取组名称
     *
     * @return group_name - 组名称
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * 设置组名称
     *
     * @param groupName 组名称
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * 获取父级组编号
     *
     * @return parent_id - 父级组编号
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 设置父级组编号
     *
     * @param parentId 父级组编号
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取0.根 1.一级 2. 二级 3.三级 ...
     *
     * @return group_leve - 0.根 1.一级 2. 二级 3.三级 ...
     */
    public Long getGroupLeve() {
        return groupLeve;
    }

    /**
     * 设置0.根 1.一级 2. 二级 3.三级 ...
     *
     * @param groupLeve 0.根 1.一级 2. 二级 3.三级 ...
     */
    public void setGroupLeve(Long groupLeve) {
        this.groupLeve = groupLeve;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Long getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}