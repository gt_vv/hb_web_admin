package vip.xiaonuo.biz.modular.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.dict.entity.HbPlayGroup;

import java.util.List;
import java.util.Map;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/7 16:33
 */
public interface HbPlayGroupMapper extends BaseMapper<HbPlayGroup> {
    int insertSelective(HbPlayGroup record);

    int updateByPrimaryKeySelective(HbPlayGroup record);

    /**
     * 通过父id查询
     *
     * @param parentId
     * @return
     */
    List<Map<String, Object>> selectByParent(@Param("parentId") Long parentId);


    /**
     * 通过分组ID查询
     * @param groupId 分组ID
     * @return
     */
    List<String> selectByGroupId(@Param("groupId") Long groupId);
}