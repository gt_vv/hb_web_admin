package vip.xiaonuo.biz.modular.dict.service;

import cn.hutool.json.JSONObject;
import vip.xiaonuo.biz.modular.dict.controller.HikController;
import vip.xiaonuo.biz.modular.dict.entity.HbCamera;
import vip.xiaonuo.biz.modular.dict.entity.HbPlayGroup;
import vip.xiaonuo.biz.modular.dict.entity.HbPlayGroupCamera;
import vip.xiaonuo.common.hikservice.HikDto;
import vip.xiaonuo.common.hikservice.HikVo;

import java.util.List;
import java.util.Map;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/4 10:31
 */
public interface IHikService {


    /**
     * 查询摄像头资源
     * @param resourcePageVo 查询参数
     * @return
     */
    HikDto.HikCameraPageDto listCamera(HikVo.ResourcePageVo resourcePageVo);

    /**
     * 获取摄像头流地址
     * @param cameraUrlVo
     * @return
     */
    HikDto.CameraUrlDto getCameraUrl(HikVo.CameraUrlVo cameraUrlVo);

    /**
     * 同步摄像头在线状态
     */
    void syncCameraLineStatus();

    /**
     * 获取摄像头的在线离线状态
     * @return
     */
    JSONObject getCameraLineCount();

    /**
     * 云台控制
     * @param cameraController
     * @return
     */
    Boolean  controlCamera(HikVo.CameraController cameraController);

    /**
     * 回放地址
     * @param cameraBackPlay
     * @return
     */
    HikDto.CameraBackPlayDto getBackPlay(HikVo.CameraBackPlay cameraBackPlay);


    /**
     * 抓图
     * @param cameraCatch
     * @return
     */
    String cameraCatch(HikVo.CameraCatch cameraCatch);


    /**
     * 开始录像
     * @param cameraVideoStart
     * @return
     */
    String cameraVideoStart(HikVo.CameraVideoStart cameraVideoStart);

    /**
     *  停止录像
     * @param cameraVideoStop
     * @return
     */
    Boolean  cameraVideoStop(HikVo.CameraVideoStop cameraVideoStop);


    /**
     * 新增播放分组
     * @param hbPlayGroup
     * @return
     */
    Boolean addCameraPlayGroup(HbPlayGroup hbPlayGroup);


    /**
     * 删除播放分组
     * @param id
     * @return
     */
    Boolean removeCameraPlayGroup(Long id);


    /**更新播放分组
     * @param hbPlayGroup
     * @return
     */
    Boolean  updateCameraPlayGroup(HbPlayGroup hbPlayGroup);


    /**
     *  新增摄像头的播放分组
     * @param groupCameraRelVo
     * @return
     */
    Boolean addCameraPlayGroupCamera(HikController.GroupCameraRelVo groupCameraRelVo);

    /**
     *  删除摄像头的播放分组
     * @param id
     * @return
     */
    Boolean removeCameraPlayGroupCamera(Long id);

    /**
     *  更新摄像头的播放分组
     * @param hbPlayGroupCamera
     * @return
     */
    Boolean updateCameraPlayGroupCamera(HbPlayGroupCamera hbPlayGroupCamera);


    /**
     * 查询
     */
    Map<String,Object> getLocalCameraList(String name);


    /**
     * 查询轮巡分组摄像头信息
     * @param name
     * @return
     */
    Map<String,Object> getPlayCameraList(String name);


    /**
     * 查询所有的摄像头
     * @return
     */
    List<HbCamera> getAllCamera(String name);


    /**
     * 同步区域
     */
    void syncArea();

    /**
     * 同步摄像机
     */
    void syncCamera();

    /**
     * 获取所有的轮巡组
     * @return
     */
    List<HbPlayGroup> getHomeCameraList();


    /**
     * @param groupId 分组ID
     * @return
     */
    List<String> getHomeCameraListMap(Long groupId);

}
