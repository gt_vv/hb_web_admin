package vip.xiaonuo.biz.modular.dict.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import vip.xiaonuo.biz.modular.dict.entity.KyEntity.AKyTask;
import vip.xiaonuo.biz.modular.dict.entity.KyEntity.KyCreateTask;
import vip.xiaonuo.biz.modular.dict.entity.dto.KyCamera;
import vip.xiaonuo.common.pojo.CommonResult;

import java.util.List;

public interface KyService {
    long residueAlg();

    CommonResult starTtask(KyCamera kyCamera);

    List<Integer> getAboutCarameAlg(String indexCode);

    Page<AKyTask> getTaskList(String keywords, Integer current, Integer size);

    boolean createTask(KyCreateTask kyCreateTask);

    boolean editTask(KyCreateTask kyCreateTask);

    boolean taskDel(Integer id);

    CommonResult task(KyCamera kyCamera);


}
