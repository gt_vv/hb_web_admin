package vip.xiaonuo.biz.modular.dict.entity.dto;

import lombok.Data;

import java.util.List;

@Data
public class KyCamera {

    /**
     * 摄像头唯一标识
     */
    private String indexCode;

    /**
     * 流地址
     */
    private String stream;



    /**
     * 算法组ID
     */
    private List<Integer> teamIds;

    private Integer type;
}
