/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * hb_algteam实体
 *
 * @author gt
 * @date  2024/03/07 10:21
 **/
@Getter
@Setter
@TableName("hb_algteam")
public class HbAlgteam {

    /** ID */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "ID", position = 1)
    private Integer id;

    /** 组名称 */
    @ApiModelProperty(value = "组名称", position = 2)
    private String teamName;

    /** 父级id */
    @ApiModelProperty(value = "父级id", position = 3)
    private Integer pId;

    /** 算法名称 */
    @ApiModelProperty(value = "算法名称", position = 4)
    private String algName;

    /** 算法id */
    @ApiModelProperty(value = "算法id", position = 5)
    private Integer algKeyId;

    /** 英文名称 code */
    @ApiModelProperty(value = "英文名称 code", position = 6)
    private String algEnName;

    /** 算法版本 */
    @ApiModelProperty(value = "算法版本", position = 7)
    private String algVersion;

    /** 别名 */
    @ApiModelProperty(value = "别名", position = 8)
    private String nickName;

    /** 算法等级 */
    @ApiModelProperty(value = "算法等级", position = 9)
    private Integer algLevel;

    /** CREATE_TIME */
    @ApiModelProperty(value = "CREATE_TIME", position = 10)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** CREATE_USER */
    @ApiModelProperty(value = "CREATE_USER", position = 11)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** UPDATE_TIME */
    @ApiModelProperty(value = "UPDATE_TIME", position = 12)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** UPDATE_USER */
    @ApiModelProperty(value = "UPDATE_USER", position = 13)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;
}
