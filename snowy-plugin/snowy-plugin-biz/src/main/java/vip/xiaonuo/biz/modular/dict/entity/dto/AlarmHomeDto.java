package vip.xiaonuo.biz.modular.dict.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/19 09:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AlarmHomeDto {

    /**
     *  摄像头名称
     */
    private String cameraName;

    /**
     *  报警时间
     */
    private Date alarmTime;

    /**
     *   报警类型
     */
    private String alarmType;

    /**
     *    报警等级
     */
    private String alarmLevel;
}
