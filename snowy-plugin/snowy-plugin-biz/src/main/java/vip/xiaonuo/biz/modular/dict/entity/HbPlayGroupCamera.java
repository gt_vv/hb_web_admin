package vip.xiaonuo.biz.modular.dict.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;

/**
 * @desc 
 * @author 张奇
 * @time  2024/3/7 16:33
 */

/**
 * 关联关系
 */
@TableName(value = "hb_play_group_camera")
@Builder
public class HbPlayGroupCamera {
    /**
     * 关联编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 播放组编号
     */
    @TableField(value = "play_group_id")
    private Long playGroupId;

    /**
     * 摄像头编号
     */
    @TableField(value = "camera_id")
    private Long cameraId;

    /**
     * 获取关联编号
     *
     * @return id - 关联编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置关联编号
     *
     * @param id 关联编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取播放组编号
     *
     * @return play_group_id - 播放组编号
     */
    public Long getPlayGroupId() {
        return playGroupId;
    }

    /**
     * 设置播放组编号
     *
     * @param playGroupId 播放组编号
     */
    public void setPlayGroupId(Long playGroupId) {
        this.playGroupId = playGroupId;
    }

    /**
     * 获取摄像头编号
     *
     * @return camera_id - 摄像头编号
     */
    public Long getCameraId() {
        return cameraId;
    }

    /**
     * 设置摄像头编号
     *
     * @param cameraId 摄像头编号
     */
    public void setCameraId(Long cameraId) {
        this.cameraId = cameraId;
    }
}