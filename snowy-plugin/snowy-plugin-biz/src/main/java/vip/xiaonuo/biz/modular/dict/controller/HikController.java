package vip.xiaonuo.biz.modular.dict.controller;

import cn.hutool.json.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.dict.entity.HbCamera;
import vip.xiaonuo.biz.modular.dict.entity.HbPlayGroup;
import vip.xiaonuo.biz.modular.dict.entity.HbPlayGroupCamera;
import vip.xiaonuo.biz.modular.dict.service.IHikService;
import vip.xiaonuo.common.hikservice.HikDto;
import vip.xiaonuo.common.hikservice.HikVo;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 海康安防管理模块
 *
 * @author 张奇
 * @desc
 * @time 2024/3/4 10:49
 */
@RestController
public class HikController {

    @Resource
    private IHikService hikService;

    /**
     * 获取摄像头的在线离线状态
     *
     * @return
     */
    @GetMapping("/biz/hik/camera/line")
    public CommonResult<JSONObject> getCameraLineCount() {
        return CommonResult.data(hikService.getCameraLineCount());
    }


    /**
     * 获取摄像头流地址
     *
     * @param cameraUrlVo
     * @return
     */
    @GetMapping("/biz/hik/camera/play/url")
    public CommonResult<List<String>> getCameraUrl(HikVo.CameraUrlVo cameraUrlVo) {
        List<String> strings = new ArrayList<>();
        for (String s : cameraUrlVo.getCameraIndexCode().split(",")) {
            HikVo.CameraUrlVo cameraUrlVo1 = new HikVo.CameraUrlVo();
            cameraUrlVo1.setCameraIndexCode(s);
            cameraUrlVo1.setProtocol(cameraUrlVo.getProtocol());
            HikDto.CameraUrlDto cameraUrl = hikService.getCameraUrl(cameraUrlVo1);
            if (cameraUrl != null && cameraUrl.getUrl() != null) {
                strings.add(cameraUrl.getUrl());
            }
        }
        return CommonResult.data(strings);
    }


    /**
     * 云台控制
     *
     * @param cameraController
     * @return
     */
    @GetMapping("/biz/hik/camera/control")
    public CommonResult<Boolean> controlCamera(HikVo.CameraController cameraController) {
        return CommonResult.data(hikService.controlCamera(cameraController));
    }


    /**
     * 回放地址
     *
     * @param cameraBackPlay
     * @return
     */
    @GetMapping("/biz/hik/camera/back/play")
    public CommonResult<HikDto.CameraBackPlayDto> getBackPlay(HikVo.CameraBackPlay cameraBackPlay) {
        return CommonResult.data(hikService.getBackPlay(cameraBackPlay));
    }

    /**
     * 抓图
     *
     * @param cameraCatch
     * @return
     */
    @GetMapping("/biz/hik/camera/pic/catch")
    public CommonResult<String> cameraCatch(HikVo.CameraCatch cameraCatch) {
        return CommonResult.data(hikService.cameraCatch(cameraCatch));
    }

    /**
     * 开始录像
     *
     * @param cameraVideoStart
     * @return
     */
    @GetMapping("/biz/hik/camera/video/start")
    public CommonResult<String> cameraVideoStart(HikVo.CameraVideoStart cameraVideoStart) {
        return CommonResult.data(hikService.cameraVideoStart(cameraVideoStart));
    }

    /**
     * 停止录像
     *
     * @param cameraVideoStop
     * @return
     */
    @GetMapping("/biz/hik/camera/video/stop")
    public CommonResult<Boolean> cameraVideoStop(HikVo.CameraVideoStop cameraVideoStop) {
        return CommonResult.data(hikService.cameraVideoStop(cameraVideoStop));
    }

    /**
     * 新增播放分组
     *
     * @param hbPlayGroup
     * @return
     */
    @PostMapping("/biz/hik/camera/play/group/add")
    public CommonResult<Boolean> addCameraPlayGroup(@RequestBody HbPlayGroup hbPlayGroup) {
        return CommonResult.data(hikService.addCameraPlayGroup(hbPlayGroup));
    }


    /**
     * 删除播放分组
     *
     * @param id 分组编号
     * @return
     */
    @GetMapping("/biz/hik/camera/play/group/remove")
    public CommonResult<Boolean> removeCameraPlayGroup(Long id) {
        return CommonResult.data(hikService.removeCameraPlayGroup(id));
    }


    /**
     * 更新播放分组
     *
     * @param hbPlayGroup
     * @return
     */
    @PostMapping("/biz/hik/camera/play/group/update")
    public CommonResult<Boolean> updateCameraPlayGroup(@RequestBody HbPlayGroup hbPlayGroup) {
        return CommonResult.data(hikService.updateCameraPlayGroup(hbPlayGroup));
    }


    /**
     * 新增摄像头的播放分组关联
     *
     * @param groupCameraRelVo
     * @return
     */
    @PostMapping("/biz/hik/camera/play/group/rel/add")
    public CommonResult<Boolean> addCameraPlayGroupCamera(@RequestBody GroupCameraRelVo groupCameraRelVo) {
        return CommonResult.data(hikService.addCameraPlayGroupCamera(groupCameraRelVo));
    }

    /**
     * 删除摄像头的播放分组关联
     *
     * @param id 关联编号
     * @return
     */
    @GetMapping("/biz/hik/camera/play/group/rel/remove")
    public CommonResult<Boolean> removeCameraPlayGroupCamera(Long id) {
        return CommonResult.data(hikService.removeCameraPlayGroupCamera(id));
    }

    /**
     * 更新摄像头的播放分组关联
     *
     * @param hbPlayGroupCamera
     * @return
     */
    @PostMapping("/biz/hik/camera/play/group/rel/update")
    public CommonResult<Boolean> updateCameraPlayGroupCamera(@RequestBody HbPlayGroupCamera hbPlayGroupCamera) {
        return CommonResult.data(hikService.updateCameraPlayGroupCamera(hbPlayGroupCamera));
    }


    /**
     * 查询普通分组摄像头
     *
     * @param name 摄像头名称
     * @return
     */
    @GetMapping("/biz/hik/camera/group/list")
    public CommonResult<List<Map<String, Object>>> getLocalCameraList(String name) {
        return CommonResult.data(Arrays.asList(hikService.getLocalCameraList(name)));
    }


    /**
     * 查询轮巡分组摄像头信息
     *
     * @param name 摄像头名称
     * @return
     */
    @GetMapping("/biz/hik/camera/group/play/list")
    public CommonResult<List<Map<String, Object>>> getPlayCameraList(String name) {
        return CommonResult.data(Arrays.asList(hikService.getPlayCameraList(name)));
    }


    /**
     * 查询所有的摄像头
     *
     * @return
     */
    @GetMapping("/biz/hik/camera/list")
    public CommonResult<List<HbCamera>> getAllCamera(String name) {
        return CommonResult.data(hikService.getAllCamera(name));
    }


    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class GroupCameraRelVo {
        /**
         * 分组ID
         */
        private Long parentId;

        /**
         * 摄像头ID(多个逗号分隔如 1,2,3)
         */
        private String cameraId;
    }


    //public static void main(String[] args) {
    //    String str = "dfsdfdsfsd";
    //    for (String s : str.split(",")) {
    //        System.out.println(s);
    //    }
    //}
}
