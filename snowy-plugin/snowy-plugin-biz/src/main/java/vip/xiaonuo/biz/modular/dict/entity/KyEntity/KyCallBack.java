package vip.xiaonuo.biz.modular.dict.entity.KyEntity;

import lombok.Data;

@Data
public class KyCallBack {
    private Integer id;

    private String name;

    private String url;
}
