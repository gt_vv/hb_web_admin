package vip.xiaonuo.biz.modular.dict.entity.KyEntity;

import lombok.Data;

@Data
public class ATaskDetailConfig {

    /**
     * 摄像头ID
     */
    private Integer cameraId;
    /**
     * 摄像头名称  indexCode
     */
    private String name;
    /**
     * 留地址
     */
    private String streamUrl;

    /**
     * 状态
     */
    private Integer status;
}
