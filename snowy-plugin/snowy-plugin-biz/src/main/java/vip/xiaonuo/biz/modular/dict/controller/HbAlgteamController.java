/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.dict.entity.HbAlgteam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgteamAddParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgteamEditParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgteamIdParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgteamPageParam;
import vip.xiaonuo.biz.modular.dict.service.HbAlgteamService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * hb_algteam控制器
 *
 * @author gt
 * @date  2024/03/07 10:21
 */
@Api(tags = "算法组控制器")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class HbAlgteamController {

    @Resource
    private HbAlgteamService hbAlgteamService;

    /**
     * 获取hb_algteam分页
     *
     * @author gt
     * @date  2024/03/07 10:21
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取算法组控制器分页-带子集树")
    //@SaCheckPermission("/biz/algteam/page")
    @GetMapping("/biz/algteam/page")
    public CommonResult<JSONObject> page(HbAlgteamPageParam hbAlgteamPageParam) {
        JSONObject page = hbAlgteamService.page(hbAlgteamPageParam);
        return CommonResult.data(page);
    }

    /**
     * 添加hb_algteam
     *
     * @author gt
     * @date  2024/03/07 10:21
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation("添加算法组")
    @CommonLog("添加算法组")
    //@SaCheckPermission("/biz/algteam/add")
    @PostMapping("/biz/algteam/add")
    public CommonResult<String> add(@RequestBody @Valid HbAlgteamAddParam hbAlgteamAddParam) {
        hbAlgteamService.add(hbAlgteamAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑hb_algteam
     *
     * @author gt
     * @date  2024/03/07 10:21
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑算法组")
    @CommonLog("编辑算法组")
    //@SaCheckPermission("/biz/algteam/edit")
    @PostMapping("/biz/algteam/edit")
    public CommonResult<String> edit(@RequestBody @Valid HbAlgteamEditParam hbAlgteamEditParam) {
        hbAlgteamService.edit(hbAlgteamEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除hb_algteam
     *
     * @author gt
     * @date  2024/03/07 10:21
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation("删除算法组")
    @CommonLog("删除算法组")
    //@SaCheckPermission("/biz/algteam/delete")
    @GetMapping("/biz/algteam/delete")
    public CommonResult<String> delete(@RequestParam Integer id) {
        hbAlgteamService.delete(id);
        return CommonResult.ok();
    }

    /**
     * 获取hb_algteam详情
     *
     * @author gt
     * @date  2024/03/07 10:21
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation("获取hb_algteam详情")
    @SaCheckPermission("/biz/algteam/detail")
    @GetMapping("/biz/algteam/detail")
    public CommonResult<HbAlgteam> detail(@Valid HbAlgteamIdParam hbAlgteamIdParam) {
        return CommonResult.data(hbAlgteamService.detail(hbAlgteamIdParam));
    }
}
