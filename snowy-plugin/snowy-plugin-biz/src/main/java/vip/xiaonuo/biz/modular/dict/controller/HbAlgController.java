/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.common.cache.CommonCacheOperator;
import vip.xiaonuo.common.pojo.CommonResult;
import vip.xiaonuo.common.pojo.CommonValidList;
import vip.xiaonuo.biz.modular.dict.entity.HbAlg;
import vip.xiaonuo.biz.modular.dict.param.HbAlgAddParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgEditParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgIdParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgPageParam;
import vip.xiaonuo.biz.modular.dict.service.HbAlgService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * hb_alg控制器
 *
 * @author 11
 * @date  2024/03/04 15:21
 */
@Api(tags = "算法模块")
@ApiSupport(author = "SNOWY_TEAM", order = 1)
@RestController
@Validated
public class HbAlgController {

    @Resource
    private HbAlgService hbAlgService;

    @Resource
    private CommonCacheOperator commonCacheOperator;

    /**
     * 获取hb_alg分页
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation("获取算法列表分页")
    //@SaCheckPermission("/biz/alg/page")
    @GetMapping("/biz/alg/page")
    public CommonResult<Page<HbAlg>> page(HbAlgPageParam hbAlgPageParam) {
        return CommonResult.data(hbAlgService.page(hbAlgPageParam));
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation("获取算法不分页")
    //@SaCheckPermission("/biz/alg/getList")
    @GetMapping("/biz/alg/getList")
    public CommonResult<Object> getList() {
        return CommonResult.data(hbAlgService.getList());
    }

    /**
     * 添加hb_alg
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    //@ApiOperationSupport(order = 2)
    //@ApiOperation("添加hb_alg")
    //@CommonLog("添加hb_alg")
    @SaCheckPermission("/biz/alg/add")
    @PostMapping("/biz/alg/add")
    public CommonResult<String> add(@RequestBody @Valid HbAlgAddParam hbAlgAddParam) {
        hbAlgService.add(hbAlgAddParam);
        return CommonResult.ok();
    }

    /**
     * 编辑hb_alg
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation("编辑hb_alg")
    @CommonLog("编辑hb_alg")
    @SaCheckPermission("/biz/alg/edit")
    @PostMapping("/biz/alg/edit")
    public CommonResult<String> edit(@RequestBody @Valid HbAlgEditParam hbAlgEditParam) {
        commonCacheOperator.put("algLevel",1);
        hbAlgService.edit(hbAlgEditParam);
        return CommonResult.ok();
    }

    /**
     * 删除hb_alg
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    //@ApiOperationSupport(order = 4)
    //@ApiOperation("删除hb_alg")
    //@CommonLog("删除hb_alg")
    @SaCheckPermission("/biz/alg/delete")
    @PostMapping("/biz/alg/delete")
    public CommonResult<String> delete(@RequestBody @Valid @NotEmpty(message = "集合不能为空")
                                                   CommonValidList<HbAlgIdParam> hbAlgIdParamList) {
        hbAlgService.delete(hbAlgIdParamList);
        return CommonResult.ok();
    }

    /**
     * 获取hb_alg详情
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    //@ApiOperationSupport(order = 5)
    //@ApiOperation("获取hb_alg详情")
    @SaCheckPermission("/biz/alg/detail")
    @GetMapping("/biz/alg/detail")
    public CommonResult<HbAlg> detail(@Valid HbAlgIdParam hbAlgIdParam) {
        return CommonResult.data(hbAlgService.detail(hbAlgIdParam));
    }
}
