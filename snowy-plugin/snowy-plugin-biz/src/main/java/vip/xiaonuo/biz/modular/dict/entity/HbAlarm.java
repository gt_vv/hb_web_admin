/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

/**
 * hb_alarm实体
 *
 * @author gt
 * @date  2024/03/04 15:08
 **/
@Getter
@Setter
@TableName("hb_alarm")
public class HbAlarm {

    /** ID */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "ID", position = 1)
    private Long id;

    /** 摄像头名称 */
    @ApiModelProperty(value = "摄像头名称", position = 2)
    private String cameraName;

    /** 报警时间 */
    @ApiModelProperty(value = "报警时间", position = 3)
    private Date alarmTime;

    /** 报警类型 */
    @ApiModelProperty(value = "报警类型", position = 4)
    private String alarmType;

    /** 报警等级 */
    @ApiModelProperty(value = "报警等级", position = 5)
    private String alarmLevel;

    /** 报警图片 */
    @ApiModelProperty(value = "报警图片", position = 6)
    private String alarmImg;

    /** CREATE_TIME */
    @ApiModelProperty(value = "CREATE_TIME", position = 7)
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** CREATE_USER */
    @ApiModelProperty(value = "CREATE_USER", position = 8)
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /** UPDATE_TIME */
    @ApiModelProperty(value = "UPDATE_TIME", position = 9)
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /** UPDATE_USER */
    @ApiModelProperty(value = "UPDATE_USER", position = 10)
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;

    private String indexCode;

    private String areaName;

    private String areaCode;
}
