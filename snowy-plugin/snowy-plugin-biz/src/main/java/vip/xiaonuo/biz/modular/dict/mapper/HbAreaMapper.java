package vip.xiaonuo.biz.modular.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.dict.entity.HbArea;

import java.util.List;
import java.util.Map;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/7 16:33
 */
public interface HbAreaMapper extends BaseMapper<HbArea> {
    int insertSelective(HbArea record);

    int updateByPrimaryKeySelective(HbArea record);

    /**
     * 查询所有
     *
     * @return
     */
    List<Map<String, Object>> selectAll();

    /**
     * 通过区域编码查询
     *
     * @param areaCode
     * @return
     */
    HbArea selectByAreaCode(@Param("areaCode") String areaCode);


}