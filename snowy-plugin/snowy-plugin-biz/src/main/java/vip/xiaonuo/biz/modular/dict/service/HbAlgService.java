/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.biz.modular.dict.entity.HbAlg;
import vip.xiaonuo.biz.modular.dict.param.HbAlgAddParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgEditParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgIdParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlgPageParam;

import java.util.List;

/**
 * hb_algService接口
 *
 * @author 11
 * @date  2024/03/04 15:21
 **/
public interface HbAlgService extends IService<HbAlg> {

    /**
     * 获取hb_alg分页
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    Page<HbAlg> page(HbAlgPageParam hbAlgPageParam);

    /**
     * 添加hb_alg
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    void add(HbAlgAddParam hbAlgAddParam);

    /**
     * 编辑hb_alg
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    void edit(HbAlgEditParam hbAlgEditParam);

    /**
     * 删除hb_alg
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    void delete(List<HbAlgIdParam> hbAlgIdParamList);

    /**
     * 获取hb_alg详情
     *
     * @author 11
     * @date  2024/03/04 15:21
     */
    HbAlg detail(HbAlgIdParam hbAlgIdParam);

    /**
     * 获取hb_alg详情
     *
     * @author 11
     * @date  2024/03/04 15:21
     **/
    HbAlg queryEntity(String id);

    Object getList();

}
