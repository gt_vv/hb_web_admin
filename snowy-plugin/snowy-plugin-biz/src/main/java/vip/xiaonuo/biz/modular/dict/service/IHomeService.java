package vip.xiaonuo.biz.modular.dict.service;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import vip.xiaonuo.biz.modular.dict.entity.dto.AlarmHomeDto;

import java.util.List;
import java.util.Map;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/19 09:51
 */
public interface IHomeService {


    /**
     * 查询最新的20条告警记录
     * @return
     */
    List<AlarmHomeDto> getHomeAlarmList();


    /**
     * 查询告警数据统计
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Map<String, Object> getHomeCountList(String startTime, String endTime);

    JSONArray areaCreamAlarm();

    JSONObject alarmCount();

}
