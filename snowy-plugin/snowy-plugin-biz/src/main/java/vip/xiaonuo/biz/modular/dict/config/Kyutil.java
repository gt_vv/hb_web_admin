package vip.xiaonuo.biz.modular.dict.config;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import vip.xiaonuo.biz.modular.dict.entity.HbAlarm;
import vip.xiaonuo.biz.modular.dict.entity.HbCamera;
import vip.xiaonuo.biz.modular.dict.mapper.HbCameraMapper;
import vip.xiaonuo.biz.modular.dict.service.HbAlarmService;
import vip.xiaonuo.common.cache.CommonCacheOperator;
import vip.xiaonuo.common.kyservice.Kyhttp;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class Kyutil {

    @Resource
    private CommonCacheOperator commonCacheOperator;

    @Resource
    private Kyhttp Kyhttp;

    @Resource
    private HbCameraMapper hbCameraMapper;

    @Resource
    private HbAlarmService hbAlarmService;

   // @Scheduled(cron = "0 0/2 * * * ?")
    @PostConstruct
    public void d() {
        //时间点获取   本月  本周  当日的 开始时间结束时间
        Calendar calendar = Calendar.getInstance();
        // 本月的开始时间和结束时间
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        setToBeginningOfDay(calendar);
        long monthStartTime = calendar.getTimeInMillis();

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DATE, -1);
        setToEndOfDay(calendar);
        long monthEndTime = calendar.getTimeInMillis();

        // 本周的开始时间和结束时间
        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        setToBeginningOfDay(calendar);
        long weekStartTime = calendar.getTimeInMillis();

        calendar.add(Calendar.DATE, 6);
        setToEndOfDay(calendar);
        long weekEndTime = calendar.getTimeInMillis();

        // 当日的开始时间和结束时间
        calendar = Calendar.getInstance();
        setToBeginningOfDay(calendar);
        long dayStartTime = calendar.getTimeInMillis();

        setToEndOfDay(calendar);
        long dayEndTime = calendar.getTimeInMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String monthStartTimeStr = sdf.format(new Date(monthStartTime));
        String monthEndTimeStr = sdf.format(new Date(monthEndTime));
        String weekStartTimeTimeStr = sdf.format(new Date(weekStartTime));
        String weekEndTimeTimeStr = sdf.format(new Date(weekEndTime));
        String dayStartTimeTimeStr = sdf.format(new Date(dayStartTime));
        String dayEndTimeTimeStr = sdf.format(new Date(dayEndTime));
        System.out.println("本月开始时间：" + monthStartTime);
        System.out.println("本月结束时间：" + monthEndTime);
        System.out.println("本周开始时间：" + weekStartTime);
        System.out.println("本周结束时间：" + weekEndTime);
        System.out.println("当日开始时间：" + dayStartTime);
        System.out.println("当日结束时间：" + dayEndTime);

        //按照时间划分告警统计
        Long monthCount = hbAlarmService.alarmStatisticsByTime(monthStartTimeStr, monthEndTimeStr);
        Long weekCount = hbAlarmService.alarmStatisticsByTime(weekStartTimeTimeStr, weekEndTimeTimeStr);
        Long dayCount = hbAlarmService.alarmStatisticsByTime(dayStartTimeTimeStr, dayEndTimeTimeStr);

        //报警统计
        List<JSONObject> jsonObjects = hbAlarmService.alarmStatistics();
        for (JSONObject jsonObject : jsonObjects) {
            System.out.println(jsonObject);
        }

    }



    public static void setToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static void setToEndOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

}
