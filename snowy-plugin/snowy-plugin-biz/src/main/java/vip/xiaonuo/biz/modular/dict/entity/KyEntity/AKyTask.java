package vip.xiaonuo.biz.modular.dict.entity.KyEntity;

import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class AKyTask {

    /**
     * 任务ID
     */
    private Integer id;

    /**
     * 任务名称
     */
    private String name;

    /***
     * 任务启用状态 0-异常 1-正常 2-注意 3-任务未启用
     */
    private String serviceHealth;

    /**
     * 任务启用状态
     */
    private String status;

    private List<AUrl> url;

    private String alg;

    private String camera;


    /**
     * 摄像头 IndexCode
     */
    private List<String> indexCodeList;

    /**
     * 算法列表
     */
    private List<Integer> algList;

    /**
     * 启用并创建
     */
    private String type;

}
