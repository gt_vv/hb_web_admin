/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.biz.modular.dict.service;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.poi.ss.formula.functions.Count;
import vip.xiaonuo.biz.modular.dict.entity.HbAlarm;
import vip.xiaonuo.biz.modular.dict.entity.dto.HbTask;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmAddParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmEditParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmIdParam;
import vip.xiaonuo.biz.modular.dict.param.HbAlarmPageParam;

import java.util.List;

/**
 * hb_alarmService接口
 *
 * @author gt
 * @date  2024/03/04 15:08
 **/
public interface HbAlarmService extends IService<HbAlarm> {

    /**
     * 获取hb_alarm分页
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    Page<HbAlarm> page(HbAlarmPageParam hbAlarmPageParam);

    /**
     * 添加hb_alarm
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    void add(HbAlarmAddParam hbAlarmAddParam);

    /**
     * 编辑hb_alarm
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    void edit(HbAlarmEditParam hbAlarmEditParam);

    /**
     * 删除hb_alarm
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    void delete(List<HbAlarmIdParam> hbAlarmIdParamList);

    /**
     * 获取hb_alarm详情
     *
     * @author gt
     * @date  2024/03/04 15:08
     */
    HbAlarm detail(HbAlarmIdParam hbAlarmIdParam);

    /**
     * 获取hb_alarm详情
     *
     * @author gt
     * @date  2024/03/04 15:08
     **/
    HbAlarm queryEntity(String id);


    List<JSONObject> alarmStatistics();

    Long alarmStatisticsByMonth(String monthStartTimeStr, String monthEndTimeStr);

    Long alarmStatisticsByTime(String monthStartTimeStr, String monthEndTimeStr);


}
