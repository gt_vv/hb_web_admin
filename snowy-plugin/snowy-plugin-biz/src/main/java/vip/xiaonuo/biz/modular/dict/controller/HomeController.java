package vip.xiaonuo.biz.modular.dict.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.biz.modular.dict.entity.HbPlayGroup;
import vip.xiaonuo.biz.modular.dict.entity.dto.AlarmHomeDto;
import vip.xiaonuo.biz.modular.dict.service.IHikService;
import vip.xiaonuo.biz.modular.dict.service.IHomeService;
import vip.xiaonuo.common.pojo.CommonResult;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 首页统计模块
 * @author 张奇
 * @desc
 * @time 2024/3/19 10:27
 */
@RestController
public class HomeController {
    @Resource
    private IHomeService homeService;

    @Resource
    private IHikService hikService;

    /**
     * 查询最新的20条告警记录
     *
     * @return
     */
    @GetMapping("/biz/alarm/home/new/list")
    public CommonResult<List<AlarmHomeDto>> getHomeAlarmList() {
        return CommonResult.data(homeService.getHomeAlarmList());
    }


    /**
     * 查询告警数据统计
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     */
    @GetMapping("/biz/alarm/home/count/list")
    public CommonResult<Map<String, Object>> getHomeCountList(String startTime, String endTime) {
        return CommonResult.data(homeService.getHomeCountList(startTime, endTime));
    }


    /**
     * 厂区 的摄像头状态 与前三的告警数
     * @return
     */
    @GetMapping("/biz/alarm/home/areaCreamAlarm")
    public CommonResult areaCreamAlarm() {
        return CommonResult.data(homeService.areaCreamAlarm());
    }


    /**
     * 天  周  月 统计报警数量
     * @return
     */
    @GetMapping("/biz/alarm/home/alarmCount")
    public CommonResult alarmCount() {
        return CommonResult.data(homeService.alarmCount());
    }


    /**
     * 获取所有的轮巡组
     * @return
     */
    @GetMapping("/biz/hik/camera/home/group/list")
    public CommonResult<List<HbPlayGroup>> getHomeCameraList() {
        return CommonResult.data(hikService.getHomeCameraList());
    }


    /**
     * 通过分组ID获取组下摄像头
     * @param groupId 分组ID
     * @return
     */
    @GetMapping("/biz/hik/camera/home/group/list/map")
    public CommonResult<List<String>> getHomeCameraListMap(Long groupId) {
        return CommonResult.data(hikService.getHomeCameraListMap(groupId));
    }
}
