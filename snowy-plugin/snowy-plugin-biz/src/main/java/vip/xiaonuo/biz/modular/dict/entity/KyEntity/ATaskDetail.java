package vip.xiaonuo.biz.modular.dict.entity.KyEntity;

import lombok.Data;

import java.util.List;

@Data
public class ATaskDetail {

    private List<ATaskDetailConfig> configList;

    /**
     * name 算法名称
     */
    private String name;

    private Integer taskId;
}
