package vip.xiaonuo.biz.modular.dict.schedul;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import vip.xiaonuo.biz.modular.dict.service.IHikService;

import javax.annotation.Resource;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/5 11:06
 */
@Component
@Slf4j
public class HikSyncKyTask {

    @Resource
    private IHikService hikService;

    /**
     * 每隔两分钟获取一次在线状态
     */
    @Scheduled(cron = "0 0/2 * * * ?")
    private void syncCameraStatus() {
        log.info("开始同步海康摄像头在线信息");
        hikService.syncCameraLineStatus();
        log.info("同步海康摄像头在线信息完成");
    }

    /**
     * 每隔两分钟同步区域信息
     */
    @Scheduled(cron = "0 0/2 * * * ?")
    private void syncArea() {
        log.info("开始同步区域信息");
        hikService.syncArea();
        log.info("同步区域信息完成");
    }


    /**
     * 每隔两分钟同步摄像头信息
     */
    @Scheduled(cron = "0 0/2 * * * ?")
    private void syncCamera() {
        log.info("开始同步海康摄像头信息");
        hikService.syncCamera();
        log.info("同步海康摄像头信息完成");
    }
}
