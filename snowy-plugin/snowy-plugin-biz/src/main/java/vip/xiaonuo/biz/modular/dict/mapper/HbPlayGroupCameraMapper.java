package vip.xiaonuo.biz.modular.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.xiaonuo.biz.modular.dict.entity.HbPlayGroupCamera;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/7 16:33
 */
public interface HbPlayGroupCameraMapper extends BaseMapper<HbPlayGroupCamera> {
    int insertSelective(HbPlayGroupCamera record);

    int updateByPrimaryKeySelective(HbPlayGroupCamera record);
}