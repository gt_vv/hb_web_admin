package vip.xiaonuo.biz.modular.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.biz.modular.dict.entity.HbCamera;

import java.util.List;
import java.util.Map;

/**
 * @author 张奇
 * @desc
 * @time 2024/3/7 16:33
 */
public interface HbCameraMapper extends BaseMapper<HbCamera> {
    int insertSelective(HbCamera record);

    int updateByPrimaryKeySelective(HbCamera record);

    /**
     * 通过区域编码查询摄像头
     *
     * @return
     */
    List<Map<String, Object>> selectByAreaCode(@Param("areaCode") String areaCode, @Param("name") String name);

    /**
     * 通过轮巡分组编号查询
     *
     * @param groupId
     * @return
     */
    List<Map<String, Object>> selectByGroupId(@Param("groupId") Long groupId, @Param("name") String name);


    /**
     * 通过索引编号查询
     *
     * @param indexCode
     * @return
     */
    HbCamera selectByIndexCode(@Param("indexCode") String indexCode);

}