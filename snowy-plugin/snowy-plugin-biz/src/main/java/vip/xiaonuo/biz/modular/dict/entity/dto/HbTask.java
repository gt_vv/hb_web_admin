package vip.xiaonuo.biz.modular.dict.entity.dto;


import lombok.Data;

@Data
public class HbTask {

    private Integer  id;

    private String taskName;

    private String caraemName;

    private String status;

    private String algorithm;

    private String indexCode;
}
