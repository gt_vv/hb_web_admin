package vip.xiaonuo.biz.modular.dict.entity.KyEntity;

import lombok.Data;

import java.util.List;

@Data
public class ATask {

    private Integer id;

    private List<String> algorithm;

    private String name;

    private Integer status;

}
