package vip.xiaonuo.biz.modular.dict.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;

/**
 * @desc 
 * @author 张奇
 * @time  2024/3/7 16:33
 */

/**
 * 区域
 */
@TableName(value = "hb_area")
@Builder
public class HbArea {
    /**
     * 区域编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 区域名称
     */
    @TableField(value = "area_name")
    private String areaName;

    /**
     * 区域编号
     */
    @TableField(value = "area_code")
    private String areaCode;

    /**
     * 父级区域编号
     */
    @TableField(value = "parent_code")
    private String parentCode;

    /**
     * 父级区域名称
     */
    @TableField(value = "parent_name")
    private String parentName;

    /**
     * 排序
     */
    @TableField(value = "sort")
    private Long sort;

    /**
     * 获取区域编号
     *
     * @return id - 区域编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置区域编号
     *
     * @param id 区域编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取区域名称
     *
     * @return area_name - 区域名称
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * 设置区域名称
     *
     * @param areaName 区域名称
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     * 获取区域编号
     *
     * @return area_code - 区域编号
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 设置区域编号
     *
     * @param areaCode 区域编号
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * 获取父级区域编号
     *
     * @return parent_code - 父级区域编号
     */
    public String getParentCode() {
        return parentCode;
    }

    /**
     * 设置父级区域编号
     *
     * @param parentCode 父级区域编号
     */
    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    /**
     * 获取父级区域名称
     *
     * @return parent_name - 父级区域名称
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * 设置父级区域名称
     *
     * @param parentName 父级区域名称
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Long getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Long sort) {
        this.sort = sort;
    }
}